# JSON Tools for Adv Mods

This repository contains a series of tools for Adv Mods to easily manage
JSON files. The main purposes of this library are:

- To import every game JSON inside the database as directly and automatically as possible.
- To create and modify every mod JSON in the fastest way possible.
- To preserve consistency in every JSON format.
- To check the validity of all JSONs in the fastest way possible.

## How to work with an Adv Mod

If you want to have a valid set of JSON files, make use of the **JSON Creator**.
There you'll find some information about how to define different behaviours through
JSON class files.

Once you have a nice JSON set, mods should run the game creation process as follows:

1. Create a database schema having the same name as the game.
All game data shall be contained within this schema.
2. Create all the essential tables and functions inside the schema, i.e. at least the following:
  - `everything` table
  - `make()` function
  - `log_event(log_level VARCHAR, log_class VARCHAR, log_content VARCHAR)` function
3. Use the **Tables Importer** to import every additional table inside the schema.
This is done by merging all the `index.json` files that each mod provides.
4. **Each mod** should import all the elements and classes that it defines,
by making use of the **Classes Importer**.
5. Once all data is imported into the database, the game can import *objects* into the game
through the **Objects Importer**.
Objects are instances of JSON classes. For a game to begin, objects should exist to provide a
playing environment and/or a storyline, such as terrains, hidden things, unique characters
or whatever you want.

The `adv-core` runs all these tasks automatically, with the exception of importing *objects*.

## JSON Creator

JSON Creator is an NCurses interface designed to facilitate the creation of JSON
files for Adv mods.

#### Usage

```
/usr/share/adv/mods/jsonlib/bin/json_creator [ JSON_DIRECTORY ]
```

If `JSON_DIRECTORY` is not provided, current working directory is assumed.

## Tables Importer

#### Usage

```
mods/jsonlib/bin/import_tables "PGSQL_CONNECTION_OPTIONS" GAME_NAME [ GAME_DIRECTORY ]
```

The first argument is **mandatory** and gives the program the PostgreSQL
options needed to connect to the game database. For example:

```
"dbname=adv host=localhost user=adv"
```

Keep in mind that in most shells the double quotes are necessary to process
it as one argument, because it contains spaces.

The second argument is **mandatory** and gives the program the game name,
corresponding to the PostgreSQL schema name.

The third argument is the game directory (where the `properties.json` file and `mods/`
directory are located). If this argument is omitted, the program will read from the
current working directory.

## Classes Importer

#### Usage

```
mods/jsonlib/bin/import_classes "PGSQL_CONNECTION_OPTIONS" GAME_NAME [ path/to/classes/dir ]
```

The first argument is **mandatory** and gives the program the PostgreSQL
options needed to connect to the game database. For example:

```
"dbname=adv host=localhost user=adv"
```

Keep in mind that in most shells the double quotes are necessary to process
it as one argument, because it contains spaces.

The second argument is **mandatory** and gives the program the game name,
corresponding to the PostgreSQL schema name.

The third argument is the directory where the `index.json` file and all
classes JSON files will be read. If this argument is omitted, the program will
read from the current working directory.

Class names starting with the `_` character are considered templates,
so the Classes Importer ignores them.

## Objects Importer

#### Usage

```
mods/jsonlib/bin/import_objects "PGSQL_CONNECTION_OPTIONS" GAME_NAME GAME_DIRECTORY \
                                [ path/to/objects/dir | path/to/objects.json ]
```

The first argument is **mandatory** and gives the program the PostgreSQL
options needed to connect to the game database. For example:

```
"dbname=adv host=localhost user=adv"
```

Keep in mind that in most shells the double quotes are necessary to process
it as one argument, because it contains spaces.

The second argument is **mandatory** and gives the program the game name,
corresponding to the PostgreSQL schema name.

The third argument is the game directory (where the `properties.json` file and `mods/`
directory are located).

The fourth argument can be a file, a directory or not given.
In case it's a directory, all objects JSON files will be read from that directory.
In case it's a file, all objects JSONs will be read from this file.
If this argument is omitted, the program will read all objects JSON files
from the current working directory. Remember:

- **If the objects path (fourth argument) is not absolute, then it'll be interpreted relative
to the** `GAME_DIRECTORY` **directory.**
- **If any read JSON file is an array, then every object inside the array will be treated as a
separated object to import to the database.**
