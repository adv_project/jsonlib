SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
SRCDIR := src
INCLUDEDIR := include
BUILDDIR := build
BINDIR := bin
MOD := jsonlib
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')
SRCEXT := cpp
VARS := -D PREFIX=\"'$(PREFIX)'\"
ifneq ("$(wildcard /usr/include/ncursesw/cursesw.h)","")
	VARS := $(VARS) -D NCURSESWDIR=\"'true'\"
endif
ifeq ("$(wildcard /usr/include/libpq-fe.h)","")
	VARS := $(VARS) -D POSTGRESQLDIR=\"'true'\"
endif
TISOURCES := $(shell echo $(SRCDIR)/{import_tables-main,TablesImporter}.$(SRCEXT))
TIOBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(TISOURCES:.$(SRCEXT)=.o))
CISOURCES := $(shell echo $(SRCDIR)/{import_classes-main,ClassesImporter}.$(SRCEXT))
CIOBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(CISOURCES:.$(SRCEXT)=.o))
OISOURCES := $(shell echo $(SRCDIR)/{import_objects-main,ObjectsImporter}.$(SRCEXT))
OIOBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(OISOURCES:.$(SRCEXT)=.o))
JCSOURCES := $(shell echo $(SRCDIR)/{json_creator-main,JsonCreator,AdvInput,Window}.$(SRCEXT))
JCOBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(JCSOURCES:.$(SRCEXT)=.o))
CC := g++ -std=c++14 $(VARS)
INCLUDE := -I $(INCLUDEDIR)
IMPORTERLIB := -lm -lpq
CREATORLIB :=  -lpthread -lncursesw

default: ti ci oi jc

.PHONY: clean

.FORCE:

ti: $(TIOBJECTS)
	@mkdir -p $(BINDIR)
	@echo -e "\e[1mLinking import_tables...\e[0m"
	@echo "CC $^ -o $(BINDIR)/import_tables $(IMPORTERLIB) $(LDFLAGS)"; $(CC) $^ -o $(BINDIR)/import_tables $(IMPORTERLIB) $(LDFLAGS)

ci: $(CIOBJECTS)
	@mkdir -p $(BINDIR)
	@echo -e "\e[1mLinking import_classes...\e[0m"
	@echo "CC $^ -o $(BINDIR)/import_classes $(IMPORTERLIB) $(LDFLAGS)"; $(CC) $^ -o $(BINDIR)/import_classes $(IMPORTERLIB) $(LDFLAGS)

oi: $(OIOBJECTS)
	@mkdir -p $(BINDIR)
	@echo -e "\e[1mLinking import_objects...\e[0m"
	@echo "CC $^ -o $(BINDIR)/import_objects $(IMPORTERLIB) $(LDFLAGS)"; $(CC) $^ -o $(BINDIR)/import_objects $(IMPORTERLIB) $(LDFLAGS)

jc: $(JCOBJECTS)
	@mkdir -p $(BINDIR)
	@echo -e "\e[1mLinking json_creator...\e[0m"
	@echo "CC $^ -o $(BINDIR)/json_creator $(CREATORLIB) $(LDFLAGS)"; $(CC) $^ -o $(BINDIR)/json_creator $(CREATORLIB) $(LDFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo "CC $(CFLAGS) $(INCLUDE) $(LDFLAGS) -c -o $@ $<"; $(CC) $(CFLAGS) $(INCLUDE) $(LDFLAGS) -c -o $@ $<

install:
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin
	test -x $(BINDIR)/json_creator && mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/assets || true
	test -x $(BINDIR)/import_tables && install -Dv -m 755 $(BINDIR)/import_tables $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin/import_tables || true
	test -x $(BINDIR)/import_classes && install -Dv -m 755 $(BINDIR)/import_classes $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin/import_classes || true
	test -x $(BINDIR)/import_objects && install -Dv -m 755 $(BINDIR)/import_objects $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin/import_objects || true
	test -x $(BINDIR)/json_creator && install -Dv -m 755 $(BINDIR)/json_creator $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/bin/json_creator && \
	install -v -m 644 assets/jcmenus.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/assets || true
	install -v -m 644 assets/properties.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:
	@echo -e "\e[1mCleaning...\e[0m"; 
	@echo "$(RM) -r $(BUILDDIR) $(BINDIR)"; $(RM) -r $(BUILDDIR) $(BINDIR)
