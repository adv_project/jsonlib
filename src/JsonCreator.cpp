/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/JsonCreator.h"
#include "JsonCreatorCallbacks.cpp"

JsonCreator::JsonCreator()
{
  setlocale(LC_ALL, "");
  do_exit = false;
  ui = false;
  new_lines_on_view = false;
  tab_mode = false;
  changes_made = false;
  input = nullptr;
  menuWindow = nullptr;
  previewWindow = nullptr;
  titleWindow = nullptr;
  preview_json_changed = true;
  view_mutex = PTHREAD_MUTEX_INITIALIZER;
}

JsonCreator::~JsonCreator()
{
  if (input != nullptr) delete input;
  if (menuWindow != nullptr) delete menuWindow;
  if (previewWindow != nullptr) delete previewWindow;
  if (titleWindow != nullptr) delete titleWindow;
}

// Read argc and argv as command line arguments and perform the requested action
int JsonCreator::exec(int argc, char **argv)
{
  std::string usage = "json_creator [ -hu ] [ WORKING_DIRECTORY ]";
  for (int i = 1; i < argc; ++i) {
    if ( (strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0) ) {
      std::cout << usage << std::endl;
      std::cout << gettext("Run the JSON Creator program at WORKING_DIRECTORY, if provided.") << std::endl;
      std::cout << gettext("Otherwise, run from the current working directory.") << std::endl;
      std::cout << "  -h, --help     " << gettext("Show this help and exit.") << std::endl;
      std::cout << "  -u, --usage    " << gettext("Show usage and exit.") << std::endl;
      return 0;
    }
    else if ( (strcmp(argv[i], "-u") == 0) || (strcmp(argv[i], "--usage") == 0) ) {
      std::cout << std::string(gettext("usage")) + ": " << usage << std::endl;
      return 0;
    }
    else {
      chdir(argv[i]);
      break;
    }
  }
  nlohmann::json ret = loadMenus();
  if (!ret["success"]) {
    std::cout << replaceAllInstances(gettext("Error reading menus file (%1)"), "%1", ret["reason"]);
    if (hasParameter(&ret, "menu")) std::cout << "; " + replaceAllInstances(gettext("Menu: %1"), "%1", ret["menu"]);
    if (hasParameter(&ret, "item")) std::cout << "; " + replaceAllInstances(gettext("Item: %1"), "%1", ret["item"]);
    std::cout << std::endl;
    return 1;
  }
  input = new AdvInput(0);
  menuWindow = new Window(0, 0, 0, 0, 0, 30, false);
  previewWindow = new Window(1, 0, 0, 0, 0, PREVIEW_BUFFER);
  titleWindow = new Window(2, 0, 0, 0, 0, 3, false);
  startUI();
  loadIndex();
  for (int i = 0; i < index["directories"].size(); ++i) {
    argument = index["directories"][i];
    callbackChangeDirectory();
  }
  if (working_directory.empty()) {
    argument = "elements";
    callbackChangeDirectory();
    argument = "classes";
    callbackChangeDirectory();
  }
  activateMenu("main");
  updateView();
  completions["attribute_types"]["numeric"] = true;
  completions["attribute_types"]["integer"] = true;
  completions["attribute_types"]["boolean"] = true;
  completions["attribute_values"]["true"] = true;
  completions["attribute_values"]["false"] = true;
  defaults["slices"]["element"] = "void";
  defaults["slices"]["mass"] = 0.0;
  defaults["slots"]["ord"] = -1;
  defaults["slots"]["slot_type"] = "placement";
  defaults["slots"]["value"] = -1.0;
  defaults["slots"]["closure"] = 0.0;
  defaults["plugs"]["ord"] = -1;
  defaults["plugs"]["slot_type"] = "placement";
  defaults["plugs"]["value"] = 0.0;
  defaults["triggers"]["task"] = "unknown";
  defaults["triggers"]["tags"] = nlohmann::json::parse("[]");
  checks.push_back("You didn't run any integrity checks yet.");
  wint_t key_int;
  wchar_t key;
  bool found;
  std::string bindings, item;
  while (!do_exit) {
    if (ui) {
      if (!active_menu.empty() && hasParameter(&menus["menus"], active_menu) && (get_wch(&key_int) != ERR)) { // Read menu bindings
        done = false;
        key = (wchar_t) key_int;
        found = false;
        for (int i = 0; i < menus["menus"][active_menu]["items"].size() && !found; ++i) {
          item = menus["menus"][active_menu]["items"][i];
          bindings = menus["items"][item]["bindings"];
          for (int c = 0; c < bindings.length() && !found; ++c)
            if (key == bindings.at(c)) {
              if (hasParameter(&menus["menus"], item)) { // Item brings you to another menu
                activateMenu(item);
                found = true;
                continue;
              }
              if (hasParameter(&menus["items"][item], "dummy")) { // Item brings you to another menu without asking for an argument
                runCallback(item);
                activateMenu(menus["items"][item]["callback"]);
                found = true;
                continue;
              }
              if (item == "quit" || (item == "quit_menu" && !changes_made)) {
                found = true;
                do_exit = true;
                continue;
              }
              callback = item;
              if (hasParameter(&menus["items"][item], "complete"))
                buildHistory(menus["items"][item]["complete"].get<std::string>());
              else
                input->history.clear();
              menuWindow->clear();
              menuWindow->addLine(Window::BOLD + " " + menus["items"][item]["title"].get<std::string>() + Window::RESET);
              menuWindow->addLine("");
              if (hasParameter(&menus["items"][item], "help_title") && menus["items"][item]["help_title"].type() == nlohmann::json::value_t::array &&
                menus["items"][item]["help_title"].size() > 0 && menus["items"][item]["help_title"][0].type() == nlohmann::json::value_t::string) {
                for (int i = 0; i < menus["items"][item]["help_title"].size(); ++i)
                  menuWindow->addLine(Window::BOLD + Window::YELLOW + menus["items"][item]["help_title"][i].get<std::string>() + Window::RESET);
                menuWindow->addLine("");
              }
              for (int i = 0; i < menus["items"][item]["help"].size(); ++i)
                menuWindow->addLine(menus["items"][item]["help"][i]);
              active_menu.clear();
              input->prompt = L"> ";
              found = true;
            }
        }
        if (!found) handleInput(key);
        updateView();
      }
      else {
        key = input->getChar();
        if (key != ERR) {
          handleInput(key);
          if (active_menu.empty() && done) { // Item brings you to another item
            done = false;
            callback = item = menus["items"][callback]["callback"];
            if (hasParameter(&menus["items"][item], "complete"))
              buildHistory(menus["items"][item]["complete"].get<std::string>());
            else
              input->history.clear();
            menuWindow->clear();
            menuWindow->addLine(Window::BOLD + " " + menus["items"][item]["title"].get<std::string>() + Window::RESET);
            menuWindow->addLine("");
            if (hasParameter(&menus["items"][item], "help_title") && menus["items"][item]["help_title"].type() == nlohmann::json::value_t::array &&
              menus["items"][item]["help_title"].size() > 0 && menus["items"][item]["help_title"][0].type() == nlohmann::json::value_t::string) {
              for (int i = 0; i < menus["items"][item]["help_title"].size(); ++i)
                menuWindow->addLine(Window::BOLD + Window::YELLOW + menus["items"][item]["help_title"][i].get<std::string>() + Window::RESET);
              menuWindow->addLine("");
            }
            for (int i = 0; i < menus["items"][item]["help"].size(); ++i)
              menuWindow->addLine(menus["items"][item]["help"][i]);
          }
          updateView();
        }
      }
    }
    usleep(MAIN_DELAY);
  }
  stopUI();
  return 0;
}

// Read the menus file, check its integrity and return an object with the result
nlohmann::json JsonCreator::loadMenus()
{
  std::ifstream menus_file;
  nlohmann::json ret;
  menus_file.open(std::string(PREFIX) + "/share/adv/mods/jsonlib/assets/jcmenus.json");
  if (menus_file.good()) {
    try {
      menus_file >> menus;
    }
    catch (...) {
      ret["success"] = false;
      ret["reason"] = "invalid_json";
      return ret;
    }
    menus_file.close();
  }
  else {
    ret["success"] = false;
    ret["reason"] = "could_not_read_file";
    return ret;
  }
  // Check menus and items integrity
  if (!hasParameter(&menus, "menus") || menus["menus"].type() != nlohmann::json::value_t::object) {
    ret["success"] = false;
    ret["reason"] = "no_menus_object";
    return ret;
  }
  if (!hasParameter(&menus, "items") || menus["items"].type() != nlohmann::json::value_t::object) {
    ret["success"] = false;
    ret["reason"] = "no_items_object";
    return ret;
  }
  // The "main" menu is the root menu of the program, so it must exist
  if (!hasParameter(&menus["menus"], "main")) {
    ret["success"] = false;
    ret["reason"] = "main_menu_does_not_exist";
    return ret;
  }
  for (auto& x : nlohmann::json::iterator_wrapper(menus["menus"])) {
    if (!hasParameter(&menus["menus"][x.key()], "title") || menus["menus"][x.key()]["title"].type() != nlohmann::json::value_t::string) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_menu_title";
      ret["menu"] = x.key();
      return ret;
    }
    if ( !hasParameter(&menus["menus"][x.key()], "help") || menus["menus"][x.key()]["help"].type() != nlohmann::json::value_t::array ||
      (menus["menus"][x.key()]["help"].size() > 0 && menus["menus"][x.key()]["help"][0].type() != nlohmann::json::value_t::string) ) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_menu_help";
      ret["menu"] = x.key();
      return ret;
    }
    if (!hasParameter(&menus["menus"][x.key()], "items") || menus["menus"][x.key()]["items"].type() != nlohmann::json::value_t::array ||
      menus["menus"][x.key()]["items"].size() == 0 || menus["menus"][x.key()]["items"][0].type() != nlohmann::json::value_t::string) {
      ret["success"] = false;
      ret["reason"] = "invalid_items_list_for_menu";
      ret["menu"] = x.key();
      return ret;
    }
    for (int i = 0; i < menus["menus"][x.key()]["items"].size(); ++i)
      if (!hasParameter(&menus["items"], menus["menus"][x.key()]["items"][i])) {
        ret["success"] = false;
        ret["reason"] = "item_not_defined";
        ret["menu"] = x.key();
        ret["item"] = menus["menus"][x.key()]["items"][i];
        return ret;
      }
  }
  for (auto& x : nlohmann::json::iterator_wrapper(menus["items"])) {
    if (!hasParameter(&menus["items"][x.key()], "title") || menus["items"][x.key()]["title"].type() != nlohmann::json::value_t::string) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_item_title";
      ret["item"] = x.key();
      return ret;
    }
    if (!hasParameter(&menus["items"][x.key()], "bindings") || menus["items"][x.key()]["bindings"].type() != nlohmann::json::value_t::string) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_item_bindings";
      ret["item"] = x.key();
      return ret;
    }
    if ( !hasParameter(&menus["items"][x.key()], "description") || menus["items"][x.key()]["description"].type() != nlohmann::json::value_t::array ||
      (menus["items"][x.key()]["description"].size() > 0 && menus["items"][x.key()]["description"][0].type() != nlohmann::json::value_t::string) ) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_item_description";
      ret["item"] = x.key();
      return ret;
    }
    if ( !hasParameter(&menus["items"][x.key()], "help") || menus["items"][x.key()]["help"].type() != nlohmann::json::value_t::array ||
      (menus["items"][x.key()]["help"].size() > 0 && menus["items"][x.key()]["help"][0].type() != nlohmann::json::value_t::string) ) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_item_help";
      ret["item"] = x.key();
      return ret;
    }
    if (!hasParameter(&menus["items"][x.key()], "callback") || menus["items"][x.key()]["callback"].type() != nlohmann::json::value_t::string) {
      ret["success"] = false;
      ret["reason"] = "invalid_or_not_existent_item_callback";
      ret["item"] = x.key();
      return ret;
    }
  }
  ret["success"] = true;
  return ret;
}

void JsonCreator::startUI()
{
  pthread_mutex_lock(&view_mutex);
  initscr();
  if ( (LINES < 8) || (COLS < 24) ) {
    endwin();
    std::cerr << std::endl << gettext("Terminal too small! Quitting") << std::endl;
    do_exit = true;
    pthread_mutex_unlock(&view_mutex);
    return;
  }
  raw();
  cbreak();
  noecho();
  timeout(0);
  start_color();
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_YELLOW, COLOR_BLACK);
  init_pair(4, COLOR_BLUE, COLOR_BLACK);
  init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(6, COLOR_CYAN, COLOR_BLACK);
  init_pair(7, COLOR_WHITE, COLOR_BLACK);
  keypad(stdscr, TRUE);
  move(0, COLS/2);
  vline(0, LINES-2);
  move(LINES-2, 0);
  hline(0, COLS);
  refresh();
  input->setCols(COLS-1);
  inputWindow = newwin(1, COLS-1, LINES-1, 0);
  menuWindow->refreshSize(LINES-2, COLS/2-1, 0, 0);
  previewWindow->refreshSize(LINES-4, COLS/2-1, 2, COLS/2+1);
  titleWindow->refreshSize(1, COLS/2-1, 0, COLS/2+1);
  ui = true;
  pthread_mutex_unlock(&view_mutex);
  updateView();
}

void JsonCreator::stopUI()
{
  pthread_mutex_lock(&view_mutex);
  ui = false;
  endwin();
  pthread_mutex_unlock(&view_mutex);
}

void JsonCreator::updateView()
{
  if (!ui) return;
  pthread_mutex_lock(&view_mutex);
  menuWindow->showWin();
  previewWindow->showWin();
  titleWindow->scrollUp(previewWindow->getMaxScroll());
  titleWindow->showWin();
  move(LINES-3, COLS-1);
  if (previewWindow->getScroll() > 0) {
    if (new_lines_on_view) wattron(stdscr, COLOR_PAIR(1));
    waddnwstr(stdscr, L"🠳", -1);
    wattrset(stdscr, 0);
  }
  else {
    waddnwstr(stdscr, L" ", -1);
    new_lines_on_view = false;
  }
  move(0, COLS-1);
  if (previewWindow->getScroll() < previewWindow->getMaxScroll())
    waddnwstr(stdscr, L"🠱", -1);
  else
    waddnwstr(stdscr, L" ", -1);
  wrefresh(stdscr);
  wattron(inputWindow, COLOR_PAIR(7));
  wmove(inputWindow, 0, 0);
  wclrtoeol(inputWindow);
  if (!input->insert) wattron(inputWindow, A_BOLD);
  wmove(inputWindow, 0, 0);
  waddnwstr(inputWindow, input->prompt.c_str(), -1);
  wattroff(inputWindow, A_BOLD);
  if (tab_mode) wattron(inputWindow, COLOR_PAIR(5));
  waddnwstr(inputWindow, input->cmdline.substr(input->offset, input->cmdline.length()-input->offset).c_str(), -1);
  wmove(inputWindow, 0, input->prompt.length() + input->cursor - 1);
  wrefresh(inputWindow);
  pthread_mutex_unlock(&view_mutex);
}

void JsonCreator::updateInputLine()
{
  if (!ui) return;
  pthread_mutex_lock(&view_mutex);
  wmove(inputWindow, 0, 0);
  wclrtoeol(inputWindow);
  if (!input->insert) wattron(inputWindow, A_BOLD);
  wmove(inputWindow, 0, 0);
  waddnwstr(inputWindow, input->prompt.c_str(), -1);
  wattroff(inputWindow, A_BOLD);
  if (tab_mode) wattron(inputWindow, COLOR_PAIR(5)); else wattroff(inputWindow, COLOR_PAIR(5));
  waddnwstr(inputWindow, input->cmdline.substr(input->offset, input->cmdline.length()-input->offset).c_str(), -1);
  wmove(inputWindow, 0, input->prompt.length() + input->cursor - 1);
  wrefresh(inputWindow);
  pthread_mutex_unlock(&view_mutex);
}

void JsonCreator::handleInput(wchar_t key)
{
  switch (key) {
    case 9: // Tab
      break;
    default:
      tab_mode = false;
      input->last_completion = 0;
      input->completion = input->cmdline;
      break;
  }
  switch (key) {
    case KEY_RESIZE:
      windowResized(SIGWINCH);
      break;
    case KEY_PPAGE:
      if (previewWindow->getLines() > 10)
        previewWindow->scrollUp(1 + previewWindow->getLines()*.75);
      else
        previewWindow->scrollUp(1);
      break;
    case CTRL('b'):
      previewWindow->scrollUp(1);
      break;
    case KEY_NPAGE:
        if (previewWindow->getLines() > 10)
          previewWindow->scrollUp(-1 - previewWindow->getLines()*.75);
        else
          previewWindow->scrollUp(-1);
      break;
    case CTRL('n'):
      previewWindow->scrollUp(-1);
      break;
    case CTRL('a'):
      previewWindow->scrollUp(previewWindow->getMaxScroll());
      break;
    case CTRL('e'):
      previewWindow->scrollUp(-previewWindow->getScroll());
      break;
    case CTRL('u'):
      input->cmdline.clear();
      input->cursor = 1;
      input->offset = 0;
      input->history_position = 0;
      runCallback(callback);
      done = true;
      break;
    case 9: // Tab
      if ( !input->cmdline.empty() && (input->cmdline.length() < (input->cursor + input->offset)) )
        tab_mode = input->handleCompletion();
      break;
    case '\n':
      if (input->cmdline.length() > 0) {
        argument = boost::locale::conv::utf_to_utf<char>(
          input->cmdline.c_str(), input->cmdline.c_str() + input->cmdline.size());
        while (argument.length() > 0 && std::isspace(argument[0])) argument.erase(0, 1);
        while (argument.length() > 0 && std::isspace(argument[argument.length()-1])) argument.erase(argument.length()-1, 1);
        if (!argument.length()) break;
        input->cmdline.clear();
        input->cursor = 1;
        input->offset = 0;
        input->history_position = 0;
        runCallback(callback);
        done = true;
      }
      break;
  }
  preview_scroll = previewWindow->getScroll();
}

void JsonCreator::activateMenu(std::string menu)
{
  if (!ui) return;
  if (menu == "quit" || (menu == "quit_menu" && !changes_made)) {
    do_exit = true;
    return;
  }
  std::string line, item, bindings;
  int c;
  if (!hasParameter(&menus["menus"], menu)) return;
  menuWindow->clear();
  menuWindow->addLine(Window::BOLD + " " + menus["menus"][menu]["title"].get<std::string>() + Window::RESET);
  if (loaded) {
    menuWindow->addLine(Window::BOLD + Window::GREEN + "JSON files loaded succesfully" + Window::RESET);
    loaded = false;
  }
  if (exported) {
    menuWindow->addLine(Window::BOLD + Window::GREEN + "JSON file/s exported succesfully" + Window::RESET);
    exported = false;
  }
  if (copied) {
    menuWindow->addLine(Window::BOLD + Window::GREEN + "JSON file copied succesfully" + Window::RESET);
    copied = false;
  }
  if (not_copied) {
    menuWindow->addLine(Window::BOLD + Window::RED + "Error making copy (JSON file already exists)" + Window::RESET);
    not_copied = false;
  }
  if (moved) {
    menuWindow->addLine(Window::BOLD + Window::GREEN + "JSON file moved succesfully" + Window::RESET);
    moved = false;
  }
  if (not_moved) {
    menuWindow->addLine(Window::BOLD + Window::RED + "Error moving (destination file already exists)" + Window::RESET);
    not_moved = false;
  }
  if (deleted) {
    menuWindow->addLine(Window::BOLD + Window::YELLOW + "JSON file/s deleted succesfully" + Window::RESET);
    deleted = false;
  }
  if ( (LINES < 32) || (COLS < 120) ) {
    menuWindow->addLine("");
    menuWindow->addLine(Window::BOLD + Window::RED + "PLEASE SET A BIGGER TERMINAL SIZE" + Window::RESET);
    menuWindow->addLine(Window::BOLD + Window::RED + "AT LEAST 120 COLUMNS AND 32 LINES" + Window::RESET);
  }
  if (menu == "main") menuWindow->addLine(std::string("Working directory: ") + Window::BOLD + working_directory + Window::RESET);
  menuWindow->addLine("");
  for (int i = 0; i < menus["menus"][menu]["items"].size(); ++i) {
    item = menus["menus"][menu]["items"][i];
    line = "[";
    bindings = menus["items"][item]["bindings"];
    for (c = 0; c < bindings.length(); ++c)
      line += Window::BOLD + Window::YELLOW + bindings.at(c) + Window::RESET + "|";
    if (c > 0) line.erase(line.end()-1);
    line += "] ";
    line += menus["items"][item]["title"];
    menuWindow->addLine(line);
    for (int i = 0; i < menus["items"][item]["description"].size(); ++i)
      menuWindow->addLine(std::string("  ") + menus["items"][item]["description"][i].get<std::string>());
  }
  if (menu == "main") {
    previewWindow->clear();
    titleWindow->clear();
    titleWindow->addLine(Window::BOLD + Window::YELLOW + "JSON Creator" + Window::RESET);
    for (int i = 0; i < menus["menus"][menu]["help"].size(); ++i)
      previewWindow->addLine(menus["menus"][menu]["help"][i]);
  }
  else if (menus["menus"][menu]["help"].size() > 0) {
    menuWindow->addLine("");
    for (int i = 0; i < menus["menus"][menu]["help"].size(); ++i)
      menuWindow->addLine(menus["menus"][menu]["help"][i]);
  }
  if (hasParameter(&menus["menus"][menu], "filter")) // We want to see the current filtered list
    showFilteredList();
  input->prompt = L"<SELECT FROM MENU>";
  active_menu = menu;
  menuWindow->scrollUp(menuWindow->getMaxScroll());
}

void JsonCreator::buildHistory(std::string complete)
{
  input->history.clear();
  if (hasParameter(&completions, complete)) {
    for (auto& x : nlohmann::json::iterator_wrapper(completions[complete])) {
      std::string str = x.key();
      input->history.push_back(boost::locale::conv::utf_to_utf<wchar_t>(
        str.c_str(), str.c_str() + str.size()));
    }
  }
  else if (complete == "class_names_in_directory" && !working_directory.empty()) {
    for (auto& x : nlohmann::json::iterator_wrapper(classes[working_directory])) {
      std::string str = x.key();
      input->history.push_back(boost::locale::conv::utf_to_utf<wchar_t>(
        str.c_str(), str.c_str() + str.size()));
    }
  }
}

void JsonCreator::showCompletions()
{
  std::string next_line;
  previewWindow->clear();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + "Loaded properties" + Window::RESET);
  for (auto& x : nlohmann::json::iterator_wrapper(completions)) {
    previewWindow->addLine(Window::BOLD + Window::YELLOW + x.key() + Window::RESET + ":");
    next_line.clear();
    for (auto& y : nlohmann::json::iterator_wrapper(completions[x.key()])) {
      if (next_line.size() + y.key().size() + 1 > 58) {
        previewWindow->addLine(next_line);
        next_line.clear();
      }
      next_line += " " + y.key();
    }
    if (!next_line.empty()) previewWindow->addLine(next_line);
    previewWindow->addLine("");
  }
  previewWindow->scrollUp(previewWindow->getMaxScroll());
}

void JsonCreator::showFilteredList()
{
  unsigned long filtered_list_size = 0;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      ++filtered_list_size;
  previewWindow->clear();
  titleWindow->clear();
  if (filter_array_index < 0) { // Show filtered list
    if (applied_filters.empty())
      titleWindow->addLine(Window::BOLD + "Complete JSON list" + Window::YELLOW + " (" + std::to_string(filtered_list_size) + ")" + Window::RESET);
    else
      titleWindow->addLine(Window::BOLD + "Filtered JSON list" + Window::YELLOW + " (" + std::to_string(filtered_list_size) + ")" + Window::RESET);
    for (int i = 0; i < applied_filters.size(); ++i)
      previewWindow->addLine(Window::YELLOW + filterText(applied_filters[i]) + Window::RESET);
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      if (!filtered_list[x.key()].empty()) {
        for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
          previewWindow->addLine(x.key() + "/" + y.key() + ".json");
        previewWindow->addLine("");
      }
  }
  else { // Show current JSON on filtered list
    if (filter_array_index < filter_array.size()) {
      working_directory = filter_array[filter_array_index]["directory"];
      setActiveJSON(filter_array[filter_array_index]["file"]);
      showActiveJSON(filter_array_index, filtered_list_size);
    }
  }
  previewWindow->scrollUp(previewWindow->getMaxScroll());
}

void JsonCreator::applyFilter(std::string filter)
{
  if (filter == "reset") {
    filtered_list.clear();
    applied_filters.clear();
    for (auto& x : nlohmann::json::iterator_wrapper(classes))
      for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()]))
        filtered_list[x.key()][y.key()] = true;
    refreshFilterArray();
    return;
  }
  else if (filter == "next_file") {
    if (++filter_array_index >= filter_array.size()) filter_array_index = 0;
    if (!filter_array.size()) filter_array_index = -1;
    return;
  }
  else if (filter == "previous_file") {
    if (--filter_array_index < 0) filter_array_index = filter_array.size() - 1;
    return;
  }
  else if (filter == "back_to_filtered_list") {
    filter_array_index = -1;
    return;
  }
  if (argument.empty()) return;
  bool found;
  if (filter == "in_directory") {
    nlohmann::json new_filtered_list;
    new_filtered_list[argument] = filtered_list[argument];
    filtered_list = new_filtered_list;
  }
  else if (filter == "not_in_directory") {
    filtered_list.erase(argument);
  }
  else if (filter == "of_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "#b#type") && classes[x.key()][y.key()]["#b#type"] != argument)
          filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "not_of_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "#b#type") && classes[x.key()][y.key()]["#b#type"] == argument)
          filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "having_class") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
        found = false;
        for (int i = 0; i < classes[x.key()][y.key()]["#c#classes"].size() && !found; ++i)
          if (classes[x.key()][y.key()]["#c#classes"][i] == argument)
            found = true;
        if (!found) filtered_list[x.key()][y.key()] = false;
      }
  }
  else if (filter == "not_having_class") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
        found = false;
        for (int i = 0; i < classes[x.key()][y.key()]["#c#classes"].size() && !found; ++i)
          if (classes[x.key()][y.key()]["#c#classes"][i] == argument)
            found = true;
          if (found) filtered_list[x.key()][y.key()] = false;
      }
  }
  else if (filter == "having_tag") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
        found = false;
        for (int i = 0; i < classes[x.key()][y.key()]["#d#tags"].size() && !found; ++i)
          if (classes[x.key()][y.key()]["#d#tags"][i] == argument)
            found = true;
          if (!found) filtered_list[x.key()][y.key()] = false;
      }
  }
  else if (filter == "not_having_tag") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
        found = false;
        for (int i = 0; i < classes[x.key()][y.key()]["#d#tags"].size() && !found; ++i)
          if (classes[x.key()][y.key()]["#d#tags"][i] == argument)
            found = true;
          if (found) filtered_list[x.key()][y.key()] = false;
      }
  }
  else if (filter == "having_attribute") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (!hasParameter(&classes[x.key()][y.key()], "attributes") || !hasParameter(&classes[x.key()][y.key()]["attributes"], argument))
          filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "not_having_attribute") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "attributes") && hasParameter(&classes[x.key()][y.key()]["attributes"], argument))
          filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "having_element_in_slice") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "slices")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["slices"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["slices"][i], "element") &&
              classes[x.key()][y.key()]["slices"][i]["element"].get<std::string>() == argument)
              found = true;
          if (!found) filtered_list[x.key()][y.key()] = false;
        }
        else filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "not_having_element_in_slice") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "slices")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["slices"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["slices"][i], "element") &&
              classes[x.key()][y.key()]["slices"][i]["element"].get<std::string>() == argument)
              found = true;
          if (found) filtered_list[x.key()][y.key()] = false;
        }
  }
  else if (filter == "having_slot_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "slots")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["slots"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["slots"][i], "slot_type") &&
              classes[x.key()][y.key()]["slots"][i]["slot_type"].get<std::string>() == argument)
              found = true;
          if (!found) filtered_list[x.key()][y.key()] = false;
        }
        else filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "not_having_slot_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "slots")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["slots"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["slots"][i], "slot_type") &&
              classes[x.key()][y.key()]["slots"][i]["slot_type"].get<std::string>() == argument)
              found = true;
          if (found) filtered_list[x.key()][y.key()] = false;
        }
  }
  else if (filter == "having_plug_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "plugs")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["plugs"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["plugs"][i], "slot_type") &&
              classes[x.key()][y.key()]["plugs"][i]["slot_type"].get<std::string>() == argument)
              found = true;
          if (!found) filtered_list[x.key()][y.key()] = false;
        }
        else filtered_list[x.key()][y.key()] = false;
  }
  else if (filter == "not_having_plug_type") {
    for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
      for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
        if (hasParameter(&classes[x.key()][y.key()], "plugs")) {
          found = false;
          for (int i = 0; i < classes[x.key()][y.key()]["plugs"].size() && !found; ++i)
            if (hasParameter(&classes[x.key()][y.key()]["plugs"][i], "slot_type") &&
              classes[x.key()][y.key()]["plugs"][i]["slot_type"].get<std::string>() == argument)
              found = true;
          if (found) filtered_list[x.key()][y.key()] = false;
        }
  }
  else { // Unknown filter
    return;
  }
  refreshFilterArray();
  nlohmann::json new_filter;
  new_filter["filter"] = filter;
  new_filter["argument"] = argument;
  applied_filters.push_back(new_filter);
}

void JsonCreator::refreshFilterArray()
{
  nlohmann::json j = filtered_list;
  filtered_list.clear();
  for (auto& x : nlohmann::json::iterator_wrapper(j))
    for (auto& y : nlohmann::json::iterator_wrapper(j[x.key()]))
      if (j[x.key()][y.key()]) filtered_list[x.key()][y.key()] = true;
  filter_array = nlohmann::json::parse("[]");
  j.clear();
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list)) {
    j["directory"] = x.key();
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      j["file"] = y.key();
      filter_array.push_back(j);
    }
  }
  filter_array_index = -1;
}

std::string JsonCreator::filterText(nlohmann::json filter)
{
  if (filter["filter"] == "in_directory") {
    return std::string("located inside the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " directory";
  }
  else if (filter["filter"] == "not_in_directory") {
    return std::string("located outside the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " directory";
  }
  else if (filter["filter"] == "of_type") {
    return std::string("being of type ") + Window::BOLD + filter["argument"].get<std::string>();
  }
  else if (filter["filter"] == "not_of_type") {
    return std::string("being of types other than ") + Window::BOLD + filter["argument"].get<std::string>();
  }
  else if (filter["filter"] == "having_class") {
    return std::string("inheriting the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " class";
  }
  else if (filter["filter"] == "not_having_class") {
    return std::string("that don't inherit the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " class";
  }
  else if (filter["filter"] == "having_tag") {
    return std::string("having the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " tag";
  }
  else if (filter["filter"] == "not_having_tag") {
    return std::string("that don't have the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " tag";
  }
  else if (filter["filter"] == "having_attribute") {
    return std::string("that have the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " attribute";
  }
  else if (filter["filter"] == "not_having_attribute") {
    return std::string("that don't have the ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " attribute";
  }
  else if (filter["filter"] == "having_element_in_slice") {
    return std::string("that have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " slice";
  }
  else if (filter["filter"] == "not_having_element_in_slice") {
    return std::string("that don't have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " slice";
  }
  else if (filter["filter"] == "having_slot_type") {
    return std::string("that have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " slot";
  }
  else if (filter["filter"] == "not_having_slot_type") {
    return std::string("that don't have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " slot";
  }
  else if (filter["filter"] == "having_plug_type") {
    return std::string("that have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " plug";
  }
  else if (filter["filter"] == "not_having_plug_type") {
    return std::string("that don't have a ") + Window::BOLD + filter["argument"].get<std::string>() + Window::NOTBOLD + " plug";
  }
  else return "";
}

void JsonCreator::showIndex()
{
  std::vector<std::string> lines = dumpIndex();
  previewWindow->clear();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + "index.json" + Window::RESET);
  for (int i = 0; i < lines.size(); ++i) previewWindow->addLine(lines[i]);
  if (preview_json_changed) {
    previewWindow->scrollUp(previewWindow->getMaxScroll());
    preview_json_changed = false;
  }
  else {
    previewWindow->scrollUp(preview_scroll);
  }
}

void JsonCreator::showActiveJSON(unsigned long index, unsigned long size)
{
  if (working_directory.empty() || active_json.empty()) return;
  std::vector<std::string> lines = dumpJSON();
  std::string title_name = active_json + ".json";
  if (hasParameter(&classes[working_directory][active_json], "triggers") && active_trigger_index >= 0 && active_trigger_index < classes[working_directory][active_json]["triggers"].size()) {
    title_name += std::string(" T:") + classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"].get<std::string>();
    if (hasParameter(&classes[working_directory][active_json]["triggers"][active_trigger_index], "conditions") && active_condition_index >= 0 &&
      active_condition_index < classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].size())
      title_name += std::string(" C:") + classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"].get<std::string>();
  }
  previewWindow->clear();
  titleWindow->clear();
  if (!size)
    titleWindow->addLine(Window::BOLD + working_directory + "/" + title_name + Window::RESET);
  else
    titleWindow->addLine(Window::BOLD + Window::YELLOW + "(" + std::to_string(index+1) + "/" + std::to_string(size) + ") " +
      Window::WHITE + working_directory + "/" + title_name + Window::RESET);
  for (int i = 0; i < lines.size(); ++i) {
    if (lines[i].find("\"triggers\":") != std::string::npos ||
      lines[i].find("\"slices\":") != std::string::npos ||
      lines[i].find("\"slots\":") != std::string::npos ||
      lines[i].find("\"plugs\":") != std::string::npos ||
      lines[i].find("\"init\":") != std::string::npos) lines[i] = Window::BOLD + Window::GREEN + lines[i] + Window::RESET;
    else if (lines[i].find("\"conditions\":") != std::string::npos) lines[i] = Window::GREEN + lines[i] + Window::RESET;
    else if (lines[i].find("\"label\":") != std::string::npos ||
      lines[i].find("\"name\":") != std::string::npos) lines[i] = Window::BLUE + lines[i] + Window::RESET;
    else if (lines[i].find("\"attributes\":") != std::string::npos) lines[i] = Window::BOLD + lines[i] + Window::RESET;
    else if (lines[i].find("\"@") != std::string::npos) lines[i] = Window::BOLD + Window::MAGENTA + lines[i] + Window::RESET;
    else if (lines[i].find("\"goto\"") != std::string::npos) lines[i] = Window::BOLD + Window::CYAN + lines[i] + Window::RESET;
    else if (lines[i].find("\"if\"") != std::string::npos ||
      lines[i].find("\"elsif\"") != std::string::npos ||
      lines[i].find("\"else\"") != std::string::npos ||
      lines[i].find("\"endif\"") != std::string::npos ||
      lines[i].find("\"negate\"") != std::string::npos) lines[i] = Window::YELLOW + lines[i] + Window::RESET;
    else if (lines[i].find("\"do_abort") != std::string::npos) lines[i] = Window::RED + lines[i] + Window::RESET;
    lines[i].insert(0, Window::RESET);
    previewWindow->addLine(lines[i]);
  }
  if (preview_json_changed) {
    previewWindow->scrollUp(previewWindow->getMaxScroll());
    preview_json_changed = false;
  }
  else {
    previewWindow->scrollUp(preview_scroll);
  }
}

void JsonCreator::setActiveJSON(std::string name)
{
  active_json = name;
  preview_json_changed = true;
}

void JsonCreator::loadIndex()
{
  std::ifstream json_file;
  std::string str = "Reading index JSON file";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  usleep(ACTIONS_DELAY);
  json_file.open("index.json");
  if (json_file.good()) {
    index.clear();
    try { json_file >> index; } catch (...) {}
    json_file.close();
  }
  if (hasParameter(&index, "modname") && (index["modname"].type() == nlohmann::json::value_t::string)) {
    index["#a#modname"] = index["modname"];
    index.erase("modname");
  }
  if (!hasParameter(&index, "attributes") || index["attributes"].type() != nlohmann::json::value_t::object)
    index["attributes"] = nlohmann::json::parse("{}");
  if (!hasParameter(&index, "directories") || index["directories"].type() != nlohmann::json::value_t::array)
    index["directories"] = nlohmann::json::parse("[]");
  if (active_menu.empty())
    input->prompt = L"> ";
  else
    input->prompt = L"<SELECT FROM MENU>";
  updateCompletions();
}

void JsonCreator::loadClasses()
{
  if (working_directory.empty()) return;
  if (hasParameter(&classes, working_directory)) return;
  nlohmann::json j;
  std::ifstream json_file;
  std::vector<std::string> file_names;
  std::string str;
  DIR *dir;
  struct dirent *entry;
  if ((dir = opendir(".")) != NULL) {
    str = std::string("Reading ") + working_directory + " directory...";
    input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
    updateInputLine();
    usleep(ACTIONS_DELAY);
    while ((entry = readdir(dir)) != NULL) {
      if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
      file_names.push_back(entry->d_name);
    }
    closedir(dir);
    for (int i = 0; i < file_names.size(); ++i) {
      str = "(" + std::to_string((int) (100.0* (double) i / (double) file_names.size())) + "%) ";
      str += std::string("Reading JSONs inside ") + working_directory + " directory";
      input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
      updateInputLine();
      usleep(ACTIONS_DELAY);
      json_file.open(file_names[i].c_str());
      if (json_file.good() && json_file.peek() != std::ifstream::traits_type::eof()) {
        j.clear();
        try { json_file >> j; } catch (...) {}
        json_file.close();
        if ( !hasParameter(&j, "id") || (j["id"].type() != nlohmann::json::value_t::string) ) continue;
        if (hasParameter(&classes[working_directory], j["id"])) continue;
        classes[working_directory][j["id"].get<std::string>()] = j;
        classes[working_directory][j["id"].get<std::string>()]["#a#id"] = j["id"];
        classes[working_directory][j["id"].get<std::string>()].erase("id");
        if (hasParameter(&j, "type")) {
          classes[working_directory][j["id"].get<std::string>()]["#b#type"] = j["type"];
          classes[working_directory][j["id"].get<std::string>()].erase("type");
        }
        if (hasParameter(&j, "classes")) {
          classes[working_directory][j["id"].get<std::string>()]["#c#classes"] = j["classes"];
          classes[working_directory][j["id"].get<std::string>()].erase("classes");
        }
        else {
          classes[working_directory][j["id"].get<std::string>()]["#c#classes"] = nlohmann::json::parse("[]");
        }
        if (hasParameter(&j, "tags")) {
          classes[working_directory][j["id"].get<std::string>()]["#d#tags"] = j["tags"];
          classes[working_directory][j["id"].get<std::string>()].erase("tags");
        }
        else {
          classes[working_directory][j["id"].get<std::string>()]["#d#tags"] = nlohmann::json::parse("[]");
        }
        if (hasParameter(&classes[working_directory][j["id"].get<std::string>()], "attributes") &&
          classes[working_directory][j["id"].get<std::string>()]["attributes"].type() != nlohmann::json::value_t::object)
          classes[working_directory][j["id"].get<std::string>()].erase("attributes");
        if (hasParameter(&classes[working_directory][j["id"].get<std::string>()], "attributes") &&
          classes[working_directory][j["id"].get<std::string>()]["attributes"].type() == nlohmann::json::value_t::object)
          for (auto& x : nlohmann::json::iterator_wrapper(classes[working_directory][j["id"].get<std::string>()]["attributes"]))
            if (!hasParameter(&index["attributes"], x.key())) {
              nlohmann::json k;
              k["type"] = "numeric";
              k["table"] = "";
              index["attributes"][x.key()] = k;
            }
        if (hasParameter(&classes[working_directory][j["id"].get<std::string>()], "triggers")) {
          for (int k = 0; k < classes[working_directory][j["id"].get<std::string>()]["triggers"].size(); ++k) {
            if (hasParameter(&classes[working_directory][j["id"].get<std::string>()]["triggers"][k], "label")) {
              classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["#a#label"] = classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["label"];
              classes[working_directory][j["id"].get<std::string>()]["triggers"][k].erase("label");
            }
            else {
              classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["#a#label"] = "unknown";
            }
            if (hasParameter(&classes[working_directory][j["id"].get<std::string>()]["triggers"][k], "conditions")) {
              for (int l = 0; l < classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"].size(); ++l) {
                if (hasParameter(&classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"][l], "label")) {
                  classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"][l]["#a#label"] = classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"][l]["label"];
                  classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"][l].erase("label");
                }
                else {
                  classes[working_directory][j["id"].get<std::string>()]["triggers"][k]["conditions"][l]["#a#label"] = "unknown";
                }
              }
            }
          }
        }
      }
    }
  }
  if (active_menu.empty())
    input->prompt = L"> ";
  else
    input->prompt = L"<SELECT FROM MENU>";
  updateCompletions();
  applyFilter("reset");
  loaded = true;
}

void JsonCreator::exportIndex()
{
  std::vector<std::string> json_dump;
  std::ofstream json_file;
  std::string str = "Exporting index JSON file";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  usleep(ACTIONS_DELAY);
  json_dump = dumpIndex();
  json_file.open("../index.json");
  if (json_file.good()) {
    for (int j = 0; j < json_dump.size(); ++j) json_file << json_dump[j] << std::endl;
    json_file.close();
  }
  exported = true;
}

void JsonCreator::exportClasses()
{
  std::vector<std::string> json_dump;
  std::vector<std::string> json_names;
  std::ofstream json_file;
  std::string str;
  for (auto& x : nlohmann::json::iterator_wrapper(classes[working_directory]))
    json_names.push_back(x.key());
  for (int i = 0; i < json_names.size(); ++i) {
    str = "(" + std::to_string((int) (100.0* (double) i / (double) json_names.size())) + "%) ";
    str += std::string("Exporting JSONs inside ") + working_directory + " directory";
    input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
    updateInputLine();
    usleep(ACTIONS_DELAY);
    setActiveJSON(json_names[i]);
    json_dump = dumpJSON();
    json_file.open(std::string(json_names[i] + ".json").c_str());
    if (json_file.good()) {
      for (int j = 0; j < json_dump.size(); ++j) json_file << json_dump[j] << std::endl;
      json_file.close();
    }
  }
  setActiveJSON();
  exported = true;
}

void JsonCreator::updateCompletions()
{
  std::string str = "Updating index properties";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  if (hasParameter(&index, "attributes") && index["attributes"].type() == nlohmann::json::value_t::object)
    for (auto& x : nlohmann::json::iterator_wrapper(index["attributes"])) {
      usleep(ACTIONS_DELAY);
      completions["attributes"][x.key()] = true;
      if (!index["attributes"][x.key()]["table"].empty())
        completions["attribute_tables"][index["attributes"][x.key()]["table"].get<std::string>()] = true;
      if (hasParameter(&index["attributes"][x.key()], "boundaries") && index["attributes"][x.key()]["boundaries"].type() == nlohmann::json::value_t::object)
        for (auto& y : nlohmann::json::iterator_wrapper(index["attributes"][x.key()]["boundaries"]))
          completions["boundaries"][y.key()] = true;
    }
  if (hasParameter(&index, "directories") && index["directories"].type() == nlohmann::json::value_t::array)
    for (int i = 0; i < index["directories"].size(); ++i) completions["directories"][index["directories"][i].get<std::string>()] = true;
  if (working_directory.empty()) return;
  str = "Updating properties inside " + working_directory + " directory";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  for (auto& x : nlohmann::json::iterator_wrapper(classes[working_directory])) {
    usleep(ACTIONS_DELAY);
    completions["class_names"][x.key()] = true;
    if (hasParameter(&classes[working_directory][x.key()], "#b#type"))
      completions["types"][classes[working_directory][x.key()]["#b#type"].get<std::string>()] = true;
    for (int i = 0; i < classes[working_directory][x.key()]["#c#classes"].size(); ++i) {
      completions["class_names"][classes[working_directory][x.key()]["#c#classes"][i].get<std::string>()] = true;
      if (classes[working_directory][x.key()]["#c#classes"][i].get<std::string>() == "element")
        completions["elements"][x.key()] = true;
    }
    for (int i = 0; i < classes[working_directory][x.key()]["#d#tags"].size(); ++i)
      completions["tag_names"][classes[working_directory][x.key()]["#d#tags"][i].get<std::string>()] = true;
    if (hasParameter(&classes[working_directory][x.key()], "attributes"))
      for (auto& y : nlohmann::json::iterator_wrapper(classes[working_directory][x.key()]["attributes"]))
        completions["attributes"][y.key()] = true;
    if (hasParameter(&classes[working_directory][x.key()], "slices")) for (int i = 0; i < classes[working_directory][x.key()]["slices"].size(); ++i) {
      if (hasParameter(&classes[working_directory][x.key()]["slices"][i], "name"))
        completions["slice_names"][classes[working_directory][x.key()]["slices"][i]["name"].get<std::string>()] = true;
      if (hasParameter(&classes[working_directory][x.key()]["slices"][i], "element"))
        completions["elements"][classes[working_directory][x.key()]["slices"][i]["element"].get<std::string>()] = true;
    }
    if (hasParameter(&classes[working_directory][x.key()], "slots")) for (int i = 0; i < classes[working_directory][x.key()]["slots"].size(); ++i) {
      if (hasParameter(&classes[working_directory][x.key()]["slots"][i], "name"))
        completions["slot_names"][classes[working_directory][x.key()]["slots"][i]["name"].get<std::string>()] = true;
      if (hasParameter(&classes[working_directory][x.key()]["slots"][i], "slot_type"))
        completions["slot_types"][classes[working_directory][x.key()]["slots"][i]["slot_type"].get<std::string>()] = true;
    }
    if (hasParameter(&classes[working_directory][x.key()], "plugs")) for (int i = 0; i < classes[working_directory][x.key()]["plugs"].size(); ++i) {
      if (hasParameter(&classes[working_directory][x.key()]["plugs"][i], "name"))
        completions["plug_names"][classes[working_directory][x.key()]["plugs"][i]["name"].get<std::string>()] = true;
      if (hasParameter(&classes[working_directory][x.key()]["plugs"][i], "slot_type"))
        completions["slot_types"][classes[working_directory][x.key()]["plugs"][i]["slot_type"].get<std::string>()] = true;
    }
    if (hasParameter(&classes[working_directory][x.key()], "triggers")) for (int i = 0; i < classes[working_directory][x.key()]["triggers"].size(); ++i) {
      if (!hasParameter(&classes[working_directory][x.key()]["triggers"][i], "#a#label"))
        classes[working_directory][x.key()]["triggers"][i]["#a#label"] = "unknown";
      completions["trigger_labels"][classes[working_directory][x.key()]["triggers"][i]["#a#label"].get<std::string>()] = true;
      if (hasParameter(&classes[working_directory][x.key()]["triggers"][i], "task"))
        completions["trigger_tasks"][classes[working_directory][x.key()]["triggers"][i]["task"].get<std::string>()] = true;
      if (hasParameter(&classes[working_directory][x.key()]["triggers"][i], "type"))
        completions["trigger_types"][classes[working_directory][x.key()]["triggers"][i]["type"].get<std::string>()] = true;
      for (int j = 0; j < classes[working_directory][x.key()]["triggers"][i]["conditions"].size(); ++j) {
        if (!hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "#a#label"))
          classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["#a#label"] = "unknown";
        completions["condition_labels"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["#a#label"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "query_name"))
          completions["query_names"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["query_name"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "test_name"))
          completions["test_names"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["test_name"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "write"))
          completions["condition_writes"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["write"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "arg"))
          completions["condition_args"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["arg"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "type"))
          completions["condition_types"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["type"].get<std::string>()] = true;
        if (hasParameter(&classes[working_directory][x.key()]["triggers"][i]["conditions"][j], "tags"))
          for (int k = 0; k < classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["tags"].size(); ++k)
            completions["tag_names"][classes[working_directory][x.key()]["triggers"][i]["conditions"][j]["tags"][k].get<std::string>()] = true;
      }
      for (int k = 0; k < classes[working_directory][x.key()]["triggers"][i]["tags"].size(); ++k)
        completions["tag_names"][classes[working_directory][x.key()]["triggers"][i]["tags"][k].get<std::string>()] = true;
    }
    if (hasParameter(&classes[working_directory][x.key()], "init")) for (int i = 0; i < classes[working_directory][x.key()]["init"].size(); ++i) {
      if (!hasParameter(&classes[working_directory][x.key()]["init"][i], "label"))
        classes[working_directory][x.key()]["init"][i]["label"] = "unknown";
      completions["init_labels"][classes[working_directory][x.key()]["init"][i]["label"].get<std::string>()] = true;
    }
    if (hasParameter(&classes[working_directory][x.key()], "ecoregions")) for (int i = 0; i < classes[working_directory][x.key()]["ecoregions"].size(); ++i)
      completions["ecoregions"][classes[working_directory][x.key()]["ecoregions"][i].get<std::string>()] = true;
  }
}

void JsonCreator::runIntegrityCheks()
{
  checks.clear();
  nlohmann::json json_class_names;
  nlohmann::json json_element_names;
  unsigned int nwarnings = 0;
  unsigned int nerrors = 0;
  bool ok = true;
  bool good;
  unsigned int nchecks = 8;
  unsigned int current_check = 1;
  std::string str;
  // Record existent classes & elements and check for duplicate ids
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running duplicated IDs check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      if (hasParameter(&json_element_names, y.key())) {
        addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
        addCheckText(std::string("Class ID \"") + y.key() + "\" already exists");
        addCheckText("inside the \"elements\" directory");
        ++nerrors;
        ok = false;
      }
      else if (hasParameter(&json_class_names, y.key())) {
        addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
        addCheckText(std::string("Class ID \"") + y.key() + "\" already exists");
        addCheckText(std::string("inside the \"") + json_class_names[y.key()].get<std::string>() + "\" directory");
        ++nerrors;
        ok = false;
      }
      else if (x.key() == "elements")
        json_element_names[y.key()] = x.key();
      else
        json_class_names[y.key()] = x.key();
    }
  if (ok) addCheckText("Duplicated IDs check passed", "success");
  // Check for attribute types & tables validity
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running attribute types & tables validity check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(index["attributes"])) {
    usleep(ACTIONS_DELAY);
    good = true;
    if (!hasParameter(&index["attributes"][x.key()], "type") ||
      index["attributes"][x.key()]["type"].type() != nlohmann::json::value_t::string ||
      (index["attributes"][x.key()]["type"].get<std::string>() != "numeric" &&
      index["attributes"][x.key()]["type"].get<std::string>() != "integer" &&
      index["attributes"][x.key()]["type"].get<std::string>() != "boolean")) {
      if (good) {
        addCheckText(std::string("Attribute \"") + x.key() + "\" error:", "error");
        good = false;
      }
      addCheckText("Invalid or not existent type");
      ++nerrors;
      ok = false;
    }
    if (!hasParameter(&index["attributes"][x.key()], "table") || index["attributes"][x.key()]["table"].get<std::string>() == "") {
      if (good) {
        addCheckText(std::string("Attribute \"") + x.key() + "\" error:", "error");
        good = false;
      }
      addCheckText("Empty or not existent table");
      ++nerrors;
      ok = false;
    }
    if (hasParameter(&index["attributes"][x.key()], "boundaries"))
      for (auto& y : nlohmann::json::iterator_wrapper(index["attributes"][x.key()]["boundaries"]))
        if (index["attributes"][x.key()]["boundaries"][y.key()].type() != nlohmann::json::value_t::number_unsigned &&
          index["attributes"][x.key()]["boundaries"][y.key()].type() != nlohmann::json::value_t::number_integer &&
          index["attributes"][x.key()]["boundaries"][y.key()].type() != nlohmann::json::value_t::number_float) {
          if (good) {
            addCheckText(std::string("Attribute \"") + x.key() + "\" error:", "error");
            good = false;
          }
          addCheckText(std::string("Bad boundary \"") + y.key() + "\"");
          ++nerrors;
          ok = false;
        }
  }
  if (ok) addCheckText("Attribute types & tables validity check passed", "success");
  // Check for classes & elements orthogonality
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running classes & elements orthogonality check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      if (x.key() == "elements" && hasParameter(&classes[x.key()][y.key()], "#b#type") &&
        classes[x.key()][y.key()]["#b#type"].get<std::string>() != "element") {
        addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
        addCheckText("Inside the \"elements\" directoty");
        addCheckText("but it's not of type \"element\"");
        ++nerrors;
        ok = false;
      }
      else if (x.key() != "elements" && hasParameter(&classes[x.key()][y.key()], "#b#type") &&
        classes[x.key()][y.key()]["#b#type"].get<std::string>() == "element") {
        addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
        addCheckText("Outside the \"elements\" directoty");
        addCheckText("but it is of type \"element\"");
        ++nerrors;
        ok = false;
      }
    }
  if (ok) addCheckText("Classes & elements orthogonality check passed", "success");
  /*
  // Check for classes & elements inheritance consistency
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running classes & elements inheritance consistency check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      if (x.key() == "elements") {
        for (int i = 0; i < classes[x.key()][y.key()]["#c#classes"].size(); ++i)
          if (!hasParameter(&json_element_names, classes[x.key()][y.key()]["#c#classes"][i])) {
            addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
            addCheckText(std::string("Inherits non-element \"") + classes[x.key()][y.key()]["#c#classes"][i].get<std::string>() + "\"");
            ++nerrors;
            ok = false;
          }
      }
      else {
        for (int i = 0; i < classes[x.key()][y.key()]["#c#classes"].size(); ++i)
          if (hasParameter(&json_element_names, classes[x.key()][y.key()]["#c#classes"][i])) {
            addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
            addCheckText(std::string("Inherits element \"") + classes[x.key()][y.key()]["#c#classes"][i].get<std::string>() + "\"");
            ++nerrors;
            ok = false;
          }
      }
    }
  if (ok) addCheckText("Classes & elements inheritance consistency check passed", "success");
  */
  // Check for elements in slices
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running elements in slices check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      good = true;
      for (int i = 0; i < classes[x.key()][y.key()]["slices"].size(); ++i)
        if (!hasParameter(&json_element_names, classes[x.key()][y.key()]["slices"][i]["element"]) &&
          hasParameter(&json_class_names, classes[x.key()][y.key()]["slices"][i]["element"])) {
          if (good) {
            addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
            good = false;
          }
          addCheckText(std::string("Has a \"") + classes[x.key()][y.key()]["slices"][i]["element"].get<std::string>() + "\" slice");
          addCheckText("but that's not an element");
          ++nerrors;
          ok = false;
        }
    }
  if (ok) addCheckText("Elements in slices check passed", "success");
  // Check for empty trigger tasks
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running empty trigger tasks check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      good = true;
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["task"].get<std::string>() == "") {
          if (good) {
            addCheckText(x.key() + "/" + y.key() + ".json error:", "error");
            good = false;
          }
          addCheckText(std::string("Trigger \"") + classes[x.key()][y.key()]["triggers"][i]["#a#label"].get<std::string>() + "\"");
          addCheckText("has an empty task");
          ++nerrors;
          ok = false;
        }
    }
  if (ok) addCheckText("Empty trigger tasks check passed", "success");
  addCheckText("");
  // Check for orphan classes
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Running orphan classes check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      good = true;
      for (int i = 0; i < classes[x.key()][y.key()]["#c#classes"].size(); ++i)
        if (!hasParameter(&json_element_names, classes[x.key()][y.key()]["#c#classes"][i]) &&
          !hasParameter(&json_class_names, classes[x.key()][y.key()]["#c#classes"][i])) {
          if (good) {
            addCheckText(x.key() + "/" + y.key() + ".json warning:", "warning");
            good = false;
          }
          addCheckText(std::string("Inherits not existent class \"") + classes[x.key()][y.key()]["#c#classes"][i].get<std::string>() + "\"");
          ++nwarnings;
          ok = false;
        }
      for (int i = 0; i < classes[x.key()][y.key()]["slices"].size(); ++i)
        if (!hasParameter(&json_element_names, classes[x.key()][y.key()]["slices"][i]["element"]) &&
          !hasParameter(&json_class_names, classes[x.key()][y.key()]["slices"][i]["element"])) {
          if (good) {
            addCheckText(x.key() + "/" + y.key() + ".json warning:", "warning");
            good = false;
          }
          addCheckText(std::string("Non-existent slice \"") + classes[x.key()][y.key()]["slices"][i]["element"].get<std::string>() + "\"");
          ++nwarnings;
          ok = false;
        }
    }
  if (!ok) {
    addCheckText("Do/es they/it exist in mods marked as dependencies?", "warning");
    addCheckText("");
  }
  // Check for defined types
  ++current_check;
  str = std::string("(") + std::to_string(current_check) + "/" + std::to_string(nchecks) + ") ";
  str += "Defined types check";
  input->prompt = boost::locale::conv::utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
  updateInputLine();
  ok = true;
  for (auto& x : nlohmann::json::iterator_wrapper(classes))
    for (auto& y : nlohmann::json::iterator_wrapper(classes[x.key()])) {
      usleep(ACTIONS_DELAY);
      if (!hasParameter(&classes[x.key()][y.key()], "#b#type")) {
        addCheckText(x.key() + "/" + y.key() + ".json warning:", "warning");
        addCheckText("Type not defined");
        ++nwarnings;
        ok = false;
      }
    }
  if (!ok) {
    addCheckText("Is/are it/they defined in mods marked as dependencies?", "warning");
    addCheckText("");
  }
  addCheckText(std::string(">> ") + std::to_string(nerrors) + " errors and " + std::to_string(nwarnings) + " warnings");
  if (!nerrors)
    addCheckText(">> Integrity check passed", "success");
  else
    addCheckText(">> Integrity check failed", "error");
}

void JsonCreator::addCheckText(std::string text, std::string type)
{
  if (type == "success") {
    text = Window::BOLD + Window::GREEN + text;
    text += Window::RESET;
  }
  else if (type == "warning") {
    text = Window::BOLD + Window::YELLOW + text;
    text += Window::RESET;
  }
  else if (type == "error") {
    text = Window::BOLD + Window::RED + text;
    text += Window::RESET;
  }
  else {
    text = Window::RESET + text;
  }
  checks.push_back(text);
}

void JsonCreator::checkDuplicatedSlugName(std::string slug)
{
  std::string initial_label = classes[working_directory][active_json][slug][active_slug_index]["name"];
  std::string label = initial_label;
  unsigned int n = 0;
  unsigned int size = classes[working_directory][active_json][slug].size();
  unsigned int i = 0;
  while (i < size)
    for (i = 0; i < size; ++i)
      if ((i != active_slug_index) && (label == classes[working_directory][active_json][slug][i]["name"])) {
        ++n;
        label = initial_label + "_" + std::to_string(n);
        break;
      }
  classes[working_directory][active_json][slug][active_slug_index]["name"] = label;
}

void JsonCreator::checkDuplicatedTriggerLabel()
{
  std::string initial_label = classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"];
  std::string label = initial_label;
  unsigned int n = 0;
  unsigned int size = classes[working_directory][active_json]["triggers"].size();
  unsigned int i = 0;
  while (i < size)
    for (i = 0; i < size; ++i)
      if ((i != active_trigger_index) && (label == classes[working_directory][active_json]["triggers"][i]["#a#label"])) {
        ++n;
        label = initial_label + "_" + std::to_string(n);
        break;
      }
  classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"] = label;
}

void JsonCreator::checkDuplicatedConditionLabel()
{
  std::string initial_label = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"];
  std::string label = initial_label;
  unsigned int n = 0;
  unsigned int size = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].size();
  unsigned int i = 0;
  while (i < size)
    for (i = 0; i < size; ++i)
      if ((i != active_condition_index) && (label == classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][i]["#a#label"])) {
        ++n;
        label = initial_label + "_" + std::to_string(n);
        break;
      }
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"] = label;
}

void JsonCreator::checkDuplicatedInitLabel()
{
  std::string initial_label = classes[working_directory][active_json]["init"][active_biome_init_index]["label"];
  std::string label = initial_label;
  unsigned int n = 0;
  unsigned int size = classes[working_directory][active_json]["init"].size();
  unsigned int i = 0;
  while (i < size)
    for (i = 0; i < size; ++i)
      if ((i != active_biome_init_index) && (label == classes[working_directory][active_json]["init"][i]["label"])) {
        ++n;
        label = initial_label + "_" + std::to_string(n);
        break;
      }
  classes[working_directory][active_json]["init"][active_biome_init_index]["label"] = label;
}

void JsonCreator::windowResized(int SIG)
{
  stopUI();
  startUI();
}

std::vector<std::string> JsonCreator::dumpIndex()
{
  std::vector<std::string> ret;
  std::string str;
  std::stringstream ss;
  ss << std::setw(2) << index << std::endl;
  while (std::getline(ss, str)) {
    str = replaceAllInstances(str, "#a#", "");
    ret.push_back(str);
  }
  return ret;
}

std::vector<std::string> JsonCreator::dumpJSON()
{
  std::vector<std::string> ret;
  if (working_directory.empty() || active_json.empty()) return ret;
  if (hasParameter(&classes[working_directory][active_json], "slices") && classes[working_directory][active_json]["slices"].is_null())
    classes[working_directory][active_json].erase("slices");
  if (hasParameter(&classes[working_directory][active_json], "slots") && classes[working_directory][active_json]["slots"].is_null())
    classes[working_directory][active_json].erase("slots");
  if (hasParameter(&classes[working_directory][active_json], "plugs") && classes[working_directory][active_json]["plugs"].is_null())
    classes[working_directory][active_json].erase("plugs");
  if (hasParameter(&classes[working_directory][active_json], "init") && classes[working_directory][active_json]["init"].is_null())
    classes[working_directory][active_json].erase("init");
  if (hasParameter(&classes[working_directory][active_json], "triggers")) {
    if (classes[working_directory][active_json]["triggers"].is_null())
      classes[working_directory][active_json].erase("triggers");
    else
      for (int i = 0; i < classes[working_directory][active_json]["triggers"].size(); ++i) {
        if (hasParameter(&classes[working_directory][active_json]["triggers"][i], "conditions") && classes[working_directory][active_json]["triggers"][i]["conditions"].is_null())
          classes[working_directory][active_json]["triggers"][i].erase("conditions");
        if (hasParameter(&classes[working_directory][active_json]["triggers"][i], "tags") && classes[working_directory][active_json]["triggers"][i]["tags"].is_null())
          classes[working_directory][active_json]["triggers"][i].erase("tags");
      }
  }
  std::string str;
  std::stringstream ss;
  ss << std::setw(2) << classes[working_directory][active_json] << std::endl;
  while (std::getline(ss, str)) {
    str = replaceAllInstances(str, "#a#", "");
    str = replaceAllInstances(str, "#b#", "");
    str = replaceAllInstances(str, "#c#", "");
    str = replaceAllInstances(str, "#d#", "");
    ret.push_back(str);
  }
  return ret;
}

std::string JsonCreator::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}
