/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/Window.h"

const std::string Window::RESET = "\0110";
const std::string Window::BOLD = "\0112";
const std::string Window::ITALIC = "\0113";
const std::string Window::UNDERLINE = "\0114";
const std::string Window::NOTBOLD = "\0115";
const std::string Window::NOTITALIC = "\0116";
const std::string Window::NOTUNDERLINE = "\0117";
const std::string Window::RED = "\011r";
const std::string Window::GREEN = "\011g";
const std::string Window::YELLOW = "\011y";
const std::string Window::BLUE = "\011b";
const std::string Window::MAGENTA = "\011m";
const std::string Window::CYAN = "\011c";
const std::string Window::WHITE = "\011w";
const std::string Window::POSITIVE = "\011p";
const std::string Window::NEGATIVE = "\011n";

Window::Window(int id, unsigned int nlines, unsigned int ncols, unsigned int begin_y, unsigned int begin_x,
               unsigned long buffer_size, bool has_border)
{
  this->id = id;
  this->buffer_size = buffer_size;
  if (has_border) {
    lines = nlines - 2;
    cols = ncols - 2;
    border = newwin(nlines, ncols, begin_y, begin_x);
    ++begin_y;
    ++begin_x;
    box(border, 0, 0);
  }
  else {
    lines = nlines;
    cols = ncols;
    border = nullptr;
  }
  window = newwin(lines, cols, begin_y, begin_x);
  scroll = 0;
}

Window::~Window()
{
  if (border != nullptr) delwin(border);
  delwin(window);
}

void Window::showWin()
{
  if (border != nullptr) {
    box(border, 0, 0);
    wnoutrefresh(border);
  }
  clearok(window, true);
  wclear(window);
  int l, i, j;
  bool done = false;
  bool end;
  std::list<std::string>::iterator it = sized_content.end();
  for (int y = 0; y < lines; ++y)
    if (it == sized_content.begin()) break; else --it;
  for (int y = 0; y < scroll; ++y)
    if (it == sized_content.begin()) break; else --it;
  for (int y = 0; y < lines; ++y) {
    if (it == sized_content.end()) break;
    end = false;
    i = 0;
    j = 0;
    l = (*it).length();
    wmove(window, y, 0);
    while (i < l) {
      if ((*it)[i] == '\011') {
        wprintw(window, (*it).substr(j, i-j).c_str());
        if (i == (l-1)) {
          if ( (it == sized_content.end()) || (y == lines) ) done = true;
          end = true;
          break;
        }
        else
          ++i;
        if ((*it)[i] == '0') wattrset(window, 0);
        else if ((*it)[i] == '1') wattrset(window, A_NORMAL);
        else if ((*it)[i] == '2') wattron(window, A_BOLD);
        else if ((*it)[i] == '3') wattron(window, A_ITALIC);
        else if ((*it)[i] == '4') wattron(window, A_UNDERLINE);
        else if ((*it)[i] == '5') wattroff(window, A_BOLD);
        else if ((*it)[i] == '6') wattroff(window, A_ITALIC);
        else if ((*it)[i] == '7') wattroff(window, A_UNDERLINE);
        else if ((*it)[i] == 'r') wattron(window, COLOR_PAIR(1));
        else if ((*it)[i] == 'g') wattron(window, COLOR_PAIR(2));
        else if ((*it)[i] == 'y') wattron(window, COLOR_PAIR(3));
        else if ((*it)[i] == 'b') wattron(window, COLOR_PAIR(4));
        else if ((*it)[i] == 'm') wattron(window, COLOR_PAIR(5));
        else if ((*it)[i] == 'c') wattron(window, COLOR_PAIR(6));
        else if ((*it)[i] == 'w') wattron(window, COLOR_PAIR(7));
        ++i;
        j = i;
      }
      else i++;
    }
    if (done) break;
    if (!end) wprintw(window, (*it).substr(j, i-j).c_str());
    ++it;
  }
  clearok(window, true);
  wnoutrefresh(window);
}

void Window::refreshSize(unsigned int nlines, unsigned int ncols, unsigned int begin_y, unsigned int begin_x)
{
  delwin(window);
  if (border != nullptr) {
    delwin(border);
    lines = nlines - 2;
    cols = ncols - 2;
    border = newwin(nlines, ncols, begin_y, begin_x);
    ++begin_y;
    ++begin_x;
    box(border, 0, 0);
  }
  else {
    lines = nlines;
    cols = ncols;
  }
  window = newwin(lines, cols, begin_y, begin_x);
  sized_content.clear();
  for (std::list<std::string>::iterator it = content.begin(); it != content.end(); ++it)
    addLineToSizedContent(*it);
  scroll = 0;
}

void Window::addLine(std::string line)
{
  content.emplace_back(line);
  if (buffer_size > 0) while (content.size() > buffer_size)
    content.erase(content.begin());
  addLineToSizedContent(line);
}

void Window::addLineToSizedContent(std::string line)
{
  std::string scape_sequences;
  if (line.length() == 0) {
    sized_content.emplace_back(line);
    if (scroll > 0) ++scroll;
  }
  else {
    while (line.length() > cols) {
      sized_content.emplace_back(scape_sequences + line.substr(0, cols));
      for (int i = 0; i < cols; ++i) {
        if ( (line[i] == '\011') && (i < line.length()-1) )
          scape_sequences += line.substr(i, 2);
      }
      if ( (line[cols-1] == '\011') && (line.length() == cols+1) ) {
        scape_sequences += line.substr(cols-1, 2);
        line.clear();
      }
      else if ( (line[cols] == '\011') && (line.length() == cols+2) ) {
        scape_sequences += line.substr(cols, 2);
        line.clear();
      }
      else line = line.substr(cols);
      if (scroll > 0) ++scroll;
    }
    if (line.length() > 0) {
      sized_content.emplace_back(scape_sequences + line);
      if (scroll > 0) ++scroll;
    }
  }
  if (buffer_size > 0) while (sized_content.size() > buffer_size)
    sized_content.erase(sized_content.begin());
}

void Window::setBufferSize(unsigned long buffer_size)
{
  this->buffer_size = buffer_size;
  if (buffer_size > 0) while (content.size() > buffer_size)
    content.erase(content.begin());
  if (buffer_size > 0) while (sized_content.size() > buffer_size)
    sized_content.erase(sized_content.begin());
}

void Window::clear() {
  content.clear();
  sized_content.clear();
  scroll = 0;
}

void Window::scrollUp(int n)
{
  if (sized_content.size() > lines) {
    scroll += n;
    if (scroll < 0) scroll = 0;
    if (scroll > sized_content.size() - lines) scroll = sized_content.size() - lines;
  }
}

int Window::getMaxScroll()
{
  int ret = sized_content.size() - lines;
  if (ret > 0) return ret; else return 0;
}
