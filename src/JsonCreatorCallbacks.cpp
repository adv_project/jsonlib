/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void JsonCreator::runCallback(std::string callback)
{
  if (hasParameter(&menus["menus"], callback)) {
    activateMenu(callback);
    return;
  }
  if (callback == "change_directory") callbackChangeDirectory();
  else if (callback == "list_properties") callbackListProperties();
  else if (callback == "select_index") callbackSelectIndex();
  else if (callback == "create_class") callbackCreateClass();
  else if (callback == "copy_class") callbackCopyClass();
  else if (callback == "move_class") callbackMoveClass();
  else if (callback == "delete_class") callbackDeleteClass();
  else if (callback == "delete_class_multiple") callbackDeleteClassMultiple();
  else if (callback == "set_type") callbackSetType();
  else if (callback == "set_type_multiple") callbackSetTypeMultiple();
  else if (callback == "remove_type") callbackRemoveType();
  else if (callback == "remove_type_multiple") callbackRemoveTypeMultiple();
  else if (callback == "add_class") callbackAddToArray("#c#classes", "class_names");
  else if (callback == "add_class_multiple") callbackAddToArrayMultiple("#c#classes", "class_names");
  else if (callback == "remove_class") callbackRemoveFromArray("#c#classes", "class_names");
  else if (callback == "remove_class_multiple") callbackRemoveFromArrayMultiple("#c#classes", "class_names");
  else if (callback == "add_tag") callbackAddToArray("#d#tags", "tag_names");
  else if (callback == "add_tag_multiple") callbackAddToArrayMultiple("#d#tags", "tag_names");
  else if (callback == "remove_tag") callbackRemoveFromArray("#d#tags", "tag_names");
  else if (callback == "remove_tag_multiple") callbackRemoveFromArrayMultiple("#d#tags", "tag_names");
  else if (callback == "set_mod_name") callbackSetModName();
  else if (callback == "select_attribute") callbackSelectAttribute();
  else if (callback == "remove_index_attribute") callbackRemoveIndexAttribute();
  else if (callback == "set_attribute_type") callbackSetAttributeField("type", "attribute_types");
  else if (callback == "set_attribute_table") callbackSetAttributeField("table", "attribute_tables");
  else if (callback == "set_boundary") callbackSetBoundary();
  else if (callback == "set_boundary_value") callbackSetBoundaryValue();
  else if (callback == "set_attribute") callbackSetAttribute(false);
  else if (callback == "set_attribute_multiple") callbackSetAttribute(true);
  else if (callback == "set_attribute_value") callbackSetAttributeValue();
  else if (callback == "set_attribute_value_multiple") callbackSetAttributeValueMultiple();
  else if (callback == "remove_attribute") callbackRemoveAttribute();
  else if (callback == "remove_attribute_multiple") callbackRemoveAttributeMultiple();
  else if (callback == "select_slice") callbackSelectSlug("slices", "slice_names");
  else if (callback == "select_slice_multiple") callbackSelectSlugMultiple("slices", "slice_names");
  else if (callback == "edit_slice_name") callbackEditSlugName("slices", "slice_names");
  else if (callback == "set_slice_element") callbackSetSlugField("slices", "element", "elements");
  else if (callback == "set_slice_element_multiple") callbackSetSlugFieldMultiple("slices", "element", "elements");
  else if (callback == "set_slice_mass") callbackSetSlugValue("slices", "mass");
  else if (callback == "set_slice_mass_multiple") callbackSetSlugValueMultiple("slices", "mass");
  else if (callback == "copy_slice") callbackCopySlug("slice");
  else if (callback == "paste_slice") callbackPasteSlug("slice");
  else if (callback == "remove_slice") callbackRemoveSlug("slices");
  else if (callback == "remove_slice_multiple") callbackRemoveSlugMultiple("slices");
  else if (callback == "select_slot") callbackSelectSlug("slots", "slot_names");
  else if (callback == "select_slot_multiple") callbackSelectSlugMultiple("slots", "slot_names");
  else if (callback == "edit_slot_name") callbackEditSlugName("slots", "slot_names");
  else if (callback == "set_slot_ord") callbackSetSlugValue("slots", "ord");
  else if (callback == "set_slot_ord_multiple") callbackSetSlugValueMultiple("slots", "ord");
  else if (callback == "set_slot_type") callbackSetSlugField("slots", "slot_type", "slot_types");
  else if (callback == "set_slot_type_multiple") callbackSetSlugFieldMultiple("slots", "slot_type", "slot_types");
  else if (callback == "set_slot_value") callbackSetSlugValue("slots", "value");
  else if (callback == "set_slot_value_multiple") callbackSetSlugValueMultiple("slots", "value");
  else if (callback == "set_slot_closure") callbackSetSlugValue("slots", "closure");
  else if (callback == "set_slot_closure_multiple") callbackSetSlugValueMultiple("slots", "closure");
  else if (callback == "copy_slot") callbackCopySlug("slot");
  else if (callback == "paste_slot") callbackPasteSlug("slot");
  else if (callback == "remove_slot") callbackRemoveSlug("slots");
  else if (callback == "remove_slot_multiple") callbackRemoveSlugMultiple("slots");
  else if (callback == "select_plug") callbackSelectSlug("plugs", "plug_names");
  else if (callback == "select_plug_multiple") callbackSelectSlugMultiple("plugs", "plug_names");
  else if (callback == "edit_plug_name") callbackEditSlugName("plugs", "plug_names");
  else if (callback == "set_plug_ord") callbackSetSlugValue("plugs", "ord");
  else if (callback == "set_plug_ord_multiple") callbackSetSlugValueMultiple("plugs", "ord");
  else if (callback == "set_plug_type") callbackSetSlugField("plugs", "slot_type", "slot_types");
  else if (callback == "set_plug_type_multiple") callbackSetSlugFieldMultiple("plugs", "slot_type", "slot_types");
  else if (callback == "set_plug_value") callbackSetSlugValue("plugs", "value");
  else if (callback == "set_plug_value_multiple") callbackSetSlugValueMultiple("plugs", "value");
  else if (callback == "copy_plug") callbackCopySlug("plug");
  else if (callback == "paste_plug") callbackPasteSlug("plug");
  else if (callback == "remove_plug") callbackRemoveSlug("plugs");
  else if (callback == "remove_plug_multiple") callbackRemoveSlugMultiple("plugs");
  else if (callback == "select_trigger") callbackSelectTrigger();
  else if (callback == "select_trigger_multiple") callbackSelectTriggerMultiple();
  else if (callback == "edit_trigger_label") callbackEditTriggerLabel();
  else if (callback == "set_task") callbackSetTriggerField("task", "trigger_tasks");
  else if (callback == "set_task_multiple") callbackSetTriggerFieldMultiple("task", "trigger_tasks");
  else if (callback == "set_trigger_type") callbackSetTriggerField("type", "trigger_types");
  else if (callback == "set_trigger_type_multiple") callbackSetTriggerFieldMultiple("type", "trigger_types");
  else if (callback == "remove_trigger_type") callbackRemoveTriggerType();
  else if (callback == "remove_trigger_type_multiple") callbackRemoveTriggerTypeMultiple();
  else if (callback == "add_trigger_tag") callbackAddTriggerTag();
  else if (callback == "add_trigger_tag_multiple") callbackAddTriggerTagMultiple();
  else if (callback == "remove_trigger_tag") callbackRemoveTriggerTag();
  else if (callback == "remove_trigger_tag_multiple") callbackRemoveTriggerTagMultiple();
  else if (callback == "move_trigger_up") callbackMoveTriggerUp();
  else if (callback == "move_trigger_down") callbackMoveTriggerDown();
  else if (callback == "copy_trigger") callbackCopyTrigger();
  else if (callback == "paste_trigger") callbackPasteTrigger();
  else if (callback == "remove_trigger") callbackRemoveTrigger();
  else if (callback == "remove_trigger_multiple") callbackRemoveTriggerMultiple();
  else if (callback == "select_condition") callbackSelectCondition();
  else if (callback == "select_condition_multiple") callbackSelectConditionMultiple();
  else if (callback == "edit_condition_label") callbackEditConditionLabel();
  else if (callback == "set_query_name") callbackSetConditionField("query_name", "query_names");
  else if (callback == "set_query_name_multiple") callbackSetConditionFieldMultiple("query_name", "query_names");
  else if (callback == "remove_query_name") callbackRemoveConditionField("query_name");
  else if (callback == "remove_query_name_multiple") callbackRemoveConditionFieldMultiple("query_name");
  else if (callback == "set_test_name") callbackSetConditionField("test_name", "test_names");
  else if (callback == "set_test_name_multiple") callbackSetConditionFieldMultiple("test_name", "test_names");
  else if (callback == "remove_test_name") callbackRemoveConditionField("test_name");
  else if (callback == "remove_test_name_multiple") callbackRemoveConditionFieldMultiple("test_name");
  else if (callback == "set_write") callbackSetConditionField("write", "condition_writes");
  else if (callback == "set_write_multiple") callbackSetConditionFieldMultiple("write", "condition_writes");
  else if (callback == "remove_write") callbackRemoveConditionField("write");
  else if (callback == "remove_write_multiple") callbackRemoveConditionFieldMultiple("write");
  else if (callback == "set_arg") callbackSetConditionField("arg", "condition_args");
  else if (callback == "set_arg_multiple") callbackSetConditionFieldMultiple("arg", "condition_args");
  else if (callback == "remove_arg") callbackRemoveConditionField("arg");
  else if (callback == "remove_arg_multiple") callbackRemoveConditionFieldMultiple("arg");
  else if (callback == "set_condition_value") callbackSetConditionValue("value");
  else if (callback == "set_condition_value_multiple") callbackSetConditionValueMultiple("value");
  else if (callback == "set_condition_amount") callbackSetConditionValue("amount");
  else if (callback == "set_condition_amount_multiple") callbackSetConditionValueMultiple("amount");
  else if (callback == "set_condition_type") callbackSetConditionField("type", "condition_types");
  else if (callback == "set_condition_type_multiple") callbackSetConditionFieldMultiple("type", "condition_types");
  else if (callback == "remove_condition_type") callbackRemoveConditionField("type");
  else if (callback == "remove_condition_type_multiple") callbackRemoveConditionFieldMultiple("type");
  else if (callback == "add_condition_tag") callbackAddConditionTag();
  else if (callback == "add_condition_tag_multiple") callbackAddConditionTagMultiple();
  else if (callback == "remove_condition_tag") callbackRemoveConditionTag();
  else if (callback == "remove_condition_tag_multiple") callbackRemoveConditionTagMultiple();
  else if (callback == "move_condition_up") callbackMoveConditionUp();
  else if (callback == "move_condition_down") callbackMoveConditionDown();
  else if (callback == "copy_condition") callbackCopyCondition();
  else if (callback == "paste_condition") callbackPasteCondition();
  else if (callback == "remove_condition") callbackRemoveCondition();
  else if (callback == "remove_condition_multiple") callbackRemoveConditionMultiple();
  else if (callback == "select_biome_init") callbackSelectBiomeInit();
  else if (callback == "select_biome_init_multiple") callbackSelectBiomeInitMultiple();
  else if (callback == "edit_init_label") callbackEditInitLabel();
  else if (callback == "select_biome_init_attribute") callbackSelectBiomeInitAttribute();
  else if (callback == "select_biome_init_attribute_multiple") callbackSelectBiomeInitAttributeMultiple();
  else if (callback == "set_biome_init_attribute_minimum") callbackSetBiomeInitAttribute("minimum");
  else if (callback == "set_biome_init_attribute_minimum_multiple") callbackSetBiomeInitAttributeMultiple("minimum");
  else if (callback == "set_biome_init_attribute_maximum") callbackSetBiomeInitAttribute("maximum");
  else if (callback == "set_biome_init_attribute_maximum_multiple") callbackSetBiomeInitAttributeMultiple("maximum");
  else if (callback == "remove_biome_init_attribute") callbackRemoveBiomeInitAttribute();
  else if (callback == "remove_biome_init_attribute_multiple") callbackRemoveBiomeInitAttributeMultiple();
  else if (callback == "copy_biome_init") callbackCopyBiomeInit();
  else if (callback == "paste_biome_init") callbackPasteBiomeInit();
  else if (callback == "remove_biome_init") callbackRemoveBiomeInit();
  else if (callback == "remove_biome_init_multiple") callbackRemoveBiomeInitMultiple();
  else if (callback == "add_ecoregion") callbackAddEcoregion();
  else if (callback == "add_ecoregion_multiple") callbackAddEcoregionMultiple();
  else if (callback == "remove_ecoregion") callbackRemoveEcoregion();
  else if (callback == "remove_ecoregion_multiple") callbackRemoveEcoregionMultiple();
  else if (callback == "check_jsons_integrity") callbackCheckJSONsIntegrity();
  else if (callback == "run_integrity_check") callbackRunIntegrityCheck();
  else if (callback == "export_index") exportIndex();
  else if (callback == "export_all_from_directory") exportClasses();
  else if (callback == "export_all") callbackExportAll();
  else if (callback.substr(0, 7) == "filter_") applyFilter(callback.substr(7, callback.length() - 7));
  if (hasParameter(&menus["items"], callback) && !hasParameter(&menus["items"][callback], "continue"))
    runCallback(menus["items"][callback]["callback"]);
}

void JsonCreator::callbackChangeDirectory()
{
  if (argument.empty()) return;
  bool found = false;
  std::string dir;
  if (!working_directory.empty()) dir = "../";
  dir += argument;
  if (chdir(dir.c_str())) {
    if (mkdir(dir.c_str(), 0755) != 0 && errno != EEXIST) {
      menuWindow->addLine(Window::BOLD + Window::RED + "Couldn't make directory " + argument + Window::RESET);
      argument.clear();
      return;
    }
    else
      chdir(dir.c_str());
  }
  working_directory = argument;
  for (int i = 0; i < index["directories"].size() && !found; ++i)
    if (index["directories"][i] == argument) found = true;
  if (!found) index["directories"].push_back(argument);
  completions["directories"][argument] = true;
  argument.clear();
  loadClasses();
}

void JsonCreator::callbackListProperties()
{
  std::string old_pwd = working_directory;
  completions.clear();
  for (auto& x : nlohmann::json::iterator_wrapper(classes)) {
    working_directory = x.key();
    updateCompletions();
  }
  working_directory = old_pwd;
  showCompletions();
}

void JsonCreator::callbackSelectIndex()
{
  preview_json_changed = true;
  showIndex();
}

void JsonCreator::callbackCreateClass()
{
  if (argument.empty()) return;
  if (!hasParameter(&classes[working_directory], argument)) {
    classes[working_directory][argument]["#a#id"] = argument;
    classes[working_directory][argument]["#c#classes"] = nlohmann::json::parse("[]");
    classes[working_directory][argument]["#d#tags"] = nlohmann::json::parse("[]");
    if (hasParameter(&deletions, working_directory) && hasParameter(&deletions[working_directory], argument))
      deletions[working_directory].erase(argument);
    changes_made = true;
  }
  completions["class_names"][argument] = true;
  setActiveJSON(argument);
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackCopyClass()
{
  if (argument.empty() || active_json.empty()) return;
  if (hasParameter(&classes[working_directory], argument)) {
    not_copied = true;
  }
  else {
    classes[working_directory][argument] = classes[working_directory][active_json];
    classes[working_directory][argument]["#a#id"] = argument;
    if (hasParameter(&deletions, working_directory) && hasParameter(&deletions[working_directory], argument))
      deletions[working_directory].erase(argument);
    copied = true;
    changes_made = true;
  }
  completions["class_names"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackMoveClass()
{
  if (argument.empty() || active_json.empty()) return;
  completions["class_names"][argument] = true;
  std::string src_dir = working_directory;
  std::string dest_dir = argument;
  callbackChangeDirectory();
  if (working_directory != dest_dir) return;
  if (hasParameter(&classes, dest_dir) && hasParameter(&classes[dest_dir], active_json)) {
    not_moved = true;
  }
  else {
    classes[dest_dir][active_json] = classes[src_dir][active_json];
    classes[src_dir].erase(active_json);
    deletions[src_dir][active_json] = true;
    if (hasParameter(&deletions, dest_dir) && hasParameter(&deletions[dest_dir], active_json))
      deletions[dest_dir].erase(active_json);
    moved = true;
    changes_made = true;
    argument = src_dir;
    callbackChangeDirectory();
  }
}

void JsonCreator::callbackDeleteClass()
{
  if (active_json.empty()) return;
  classes[working_directory].erase(active_json);
  deletions[working_directory][active_json] = true;
  argument.clear();
  setActiveJSON();
  deleted = true;
  changes_made = true;
}

void JsonCreator::callbackDeleteClassMultiple()
{
  if (filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      classes[x.key()].erase(y.key());
      deletions[x.key()][y.key()] = true;
    }
  deleted = true;
  changes_made = true;
}

void JsonCreator::callbackSetType()
{
  if (argument.empty() || active_json.empty()) return;
  classes[working_directory][active_json]["#b#type"] = argument;
  completions["types"][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetTypeMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      classes[x.key()][y.key()]["#b#type"] = argument;
  completions["types"][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveType()
{
  if (active_json.empty()) return;
  classes[working_directory][active_json].erase("#b#type");
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveTypeMultiple()
{
  if (filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      classes[x.key()][y.key()].erase("#b#type");
  changes_made = true;
}

void JsonCreator::callbackAddToArray(std::string field_name, std::string completion_name)
{
  if (argument.empty() || active_json.empty()) return;
  bool found = false;
  for (int i = 0; i < classes[working_directory][active_json][field_name].size() && !found; ++i)
    if (classes[working_directory][active_json][field_name][i] == argument)
      found = true;
  if (!found) classes[working_directory][active_json][field_name].push_back(argument);
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackAddToArrayMultiple(std::string field_name, std::string completion_name)
{
  bool found;
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      found = false;
      for (int i = 0; i < classes[x.key()][y.key()][field_name].size() && !found; ++i)
        if (classes[x.key()][y.key()][field_name][i] == argument)
          found = true;
        if (!found) classes[x.key()][y.key()][field_name].push_back(argument);
    }
  completions[completion_name][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveFromArray(std::string field_name, std::string completion_name)
{
  if (argument.empty() || active_json.empty()) return;
  for (int i = 0; i < classes[working_directory][active_json][field_name].size(); ++i)
    if (classes[working_directory][active_json][field_name][i] == argument) {
      classes[working_directory][active_json][field_name].erase(i);
      break;
    }
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveFromArrayMultiple(std::string field_name, std::string completion_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      for (int i = 0; i < classes[x.key()][y.key()][field_name].size(); ++i)
        if (classes[x.key()][y.key()][field_name][i] == argument) {
          classes[x.key()][y.key()][field_name].erase(i);
          break;
        }
  completions[completion_name][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSetModName()
{
  if (argument.empty()) return;
  index["#a#modname"] = argument;
  showIndex();
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSelectAttribute()
{
  if (argument.empty()) return;
  active_attribute = argument;
  completions["attributes"][argument] = true;
  if (!hasParameter(&index["attributes"], argument)) {
    nlohmann::json j;
    j["type"] = "numeric";
    j["table"] = "";
    index["attributes"][argument] = j;
    showIndex();
    changes_made = true;
  }
  argument.clear();
}

void JsonCreator::callbackSetAttributeField(std::string field_name, std::string completion_name)
{
  if (active_attribute.empty()) return;
  index["attributes"][active_attribute][field_name] = argument;
  if (completion_name != "attribute_types") completions[completion_name][argument] = true;
  argument.clear();
  showIndex();
  changes_made = true;
}

void JsonCreator::callbackRemoveIndexAttribute()
{
  if (active_attribute.empty()) return;
  if (hasParameter(&index["attributes"], active_attribute)) index["attributes"].erase(active_attribute);
  active_attribute.clear();
  showIndex();
  changes_made = true;
}

void JsonCreator::callbackSetBoundary()
{
  if (argument.empty() || active_attribute.empty()) return;
  completions["boundaries"][argument] = true;
  active_boundary = argument;
  argument.clear();
}

void JsonCreator::callbackSetBoundaryValue()
{
  if (active_attribute.empty() || active_boundary.empty()) return;
  try {
    index["attributes"][active_attribute]["boundaries"][active_boundary] = std::stod(argument);
  } catch (...) {
    if (hasParameter(&index["attributes"][active_attribute], "boundaries")) {
      if (hasParameter(&index["attributes"][active_attribute]["boundaries"], active_boundary))
        index["attributes"][active_attribute]["boundaries"].erase(active_boundary);
      if (index["attributes"][active_attribute]["boundaries"].empty())
        index["attributes"][active_attribute].erase("boundaries");
    }
  }
  argument.clear();
  active_boundary.clear();
  showIndex();
  changes_made = true;
}

void JsonCreator::callbackSetAttribute(bool multiple)
{
  if (argument.empty()) return;
  if (!multiple && active_json.empty()) return;
  if (multiple && filtered_list.empty()) return;
  active_attribute = argument;
  completions["attributes"][argument] = true;
  if (!hasParameter(&index["attributes"], argument)) {
    nlohmann::json j;
    j["type"] = "numeric";
    j["table"] = "";
    index["attributes"][argument] = j;
    changes_made = true;
  }
  argument.clear();
}

void JsonCreator::callbackSetAttributeValue()
{
  if (argument.empty() || active_json.empty() || active_attribute.empty()) return;
  if (argument == "true")
    classes[working_directory][active_json]["attributes"][active_attribute] = true;
  else if (argument == "false")
    classes[working_directory][active_json]["attributes"][active_attribute] = false;
  else
    try {
      classes[working_directory][active_json]["attributes"][active_attribute] = (double) std::stod(argument);
    } catch (...) {}
  argument.clear();
  active_attribute.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetAttributeValueMultiple()
{
  if (argument.empty() || filtered_list.empty() || active_attribute.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      if (argument == "true")
        classes[x.key()][y.key()]["attributes"][active_attribute] = true;
      else if (argument == "false")
        classes[x.key()][y.key()]["attributes"][active_attribute] = false;
      else
        try {
          classes[x.key()][y.key()]["attributes"][active_attribute] = (double) std::stod(argument);
        } catch (...) {}
    }
  argument.clear();
  active_attribute.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveAttribute()
{
  if (argument.empty() || active_json.empty()) return;
  if (hasParameter(&classes[working_directory][active_json], "attributes")) {
    if (hasParameter(&classes[working_directory][active_json]["attributes"], argument))
      classes[working_directory][active_json]["attributes"].erase(argument);
    if (classes[working_directory][active_json]["attributes"].empty())
      classes[working_directory][active_json].erase("attributes");
  }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveAttributeMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      if (hasParameter(&classes[x.key()][y.key()], "attributes")) {
        if (hasParameter(&classes[x.key()][y.key()]["attributes"], argument))
          classes[x.key()][y.key()]["attributes"].erase(argument);
        if (classes[x.key()][y.key()]["attributes"].empty())
          classes[x.key()][y.key()].erase("attributes");
      }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSelectSlug(std::string field_name, std::string completion_name)
{
  if (argument.empty() || active_json.empty()) return;
  unsigned int i;
  bool found = false;
  active_slug = argument;
  for (i = 0; i < classes[working_directory][active_json][field_name].size(); ++i)
    if (classes[working_directory][active_json][field_name][i]["name"] == argument) {
      found = true;
      break;
    }
  active_slug_index = i;
  if (!found) {
    nlohmann::json j = defaults[field_name];
    j["name"] = argument;
    for (i = 0; i < classes[working_directory][active_json][field_name].size(); ++i)
      if (j["ord"] < classes[working_directory][active_json][field_name][i]["ord"]) break;
    if (i)
      classes[working_directory][active_json][field_name].insert(classes[working_directory][active_json][field_name].begin() + i, j);
    else
      classes[working_directory][active_json][field_name].push_back(j);
    active_slug_index = i;
    changes_made = true;
  }
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackSelectSlugMultiple(std::string field_name, std::string completion_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  unsigned int i;
  bool found = false;
  active_slug = argument;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (i = 0; i < classes[x.key()][y.key()][field_name].size() && !found; ++i)
        if (classes[x.key()][y.key()][field_name][i]["name"] == argument)
          found = true;
      if (!found) {
        nlohmann::json j = defaults[field_name];
        j["name"] = argument;
        for (i = 0; i < classes[x.key()][y.key()][field_name].size(); ++i)
          if (j["ord"] < classes[x.key()][y.key()][field_name][i]["ord"]) break;
        if (i)
          classes[x.key()][y.key()][field_name].insert(classes[x.key()][y.key()][field_name].begin() + i, j);
        else
          classes[x.key()][y.key()][field_name].push_back(j);
        changes_made = true;
      }
    }
  completions[completion_name][argument] = true;
  argument.clear();
}

void JsonCreator::callbackEditSlugName(std::string slug, std::string completion_name)
{
  if (argument.empty() || active_json.empty() || active_slug_index < 0) return;
  classes[working_directory][active_json][slug][active_slug_index]["name"] = argument;
  checkDuplicatedSlugName(slug);
  argument = classes[working_directory][active_json][slug][active_slug_index]["name"];
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetSlugField(std::string slug, std::string field_name, std::string completion_name)
{
  if (argument.empty() || active_json.empty()) return;
  classes[working_directory][active_json][slug][active_slug_index][field_name] = argument;
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetSlugFieldMultiple(std::string slug, std::string field_name, std::string completion_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      for (int i = 0; i < classes[x.key()][y.key()][slug].size(); ++i)
        if (classes[x.key()][y.key()][slug][i]["name"] == active_slug) {
          classes[x.key()][y.key()][slug][i][field_name] = argument;
          break;
        }
  completions[completion_name][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSetSlugValue(std::string slug, std::string field_name)
{
  if (argument.empty() || active_json.empty()) return;
  try {
    classes[working_directory][active_json][slug][active_slug_index][field_name] = std::stod(argument);
  } catch (...) {}
  if (field_name == "ord") {
    nlohmann::json j = classes[working_directory][active_json][slug][active_slug_index];
    argument = j["name"];
    classes[working_directory][active_json][slug].erase(active_slug_index);
    unsigned int i;
    for (i = 0; i < classes[working_directory][active_json][slug].size(); ++i)
      if (j["ord"] < classes[working_directory][active_json][slug][i]["ord"]) break;
    classes[working_directory][active_json][slug].insert(classes[working_directory][active_json][slug].begin() + i, j);
    for (int i = 0; i < classes[working_directory][active_json][slug].size(); ++i)
      if (classes[working_directory][active_json][slug][i]["name"] == argument) {
        active_slug_index = i;
        break;
      }
  }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetSlugValueMultiple(std::string slug, std::string field_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  unsigned int i = 0;
  double n;
  try {
    n = std::stod(argument);
  } catch (...) {}
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      for (i = 0; i < classes[x.key()][y.key()][slug].size(); ++i)
        if (classes[x.key()][y.key()][slug][i]["name"] == active_slug) {
          classes[x.key()][y.key()][slug][i][field_name] = n;
          if (field_name == "ord") {
            nlohmann::json j = classes[x.key()][y.key()][slug][i];
            classes[x.key()][y.key()][slug].erase(i);
            for (i = 0; i < classes[x.key()][y.key()][slug].size(); ++i)
              if (j["ord"] < classes[x.key()][y.key()][slug][i]["ord"]) break;
            classes[x.key()][y.key()][slug].insert(classes[x.key()][y.key()][slug].begin() + i, j);
          }
          break;
        }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackCopySlug(std::string slug)
{
  if (active_json.empty() || active_slug_index < 0) return;
  std::string plural = slug;
  plural.append("s");
  clipboard[slug] = classes[working_directory][active_json][plural][active_slug_index];
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Copied " + slug + " to clipboard" + Window::RESET);
}

void JsonCreator::callbackPasteSlug(std::string slug)
{
  if (active_json.empty() || active_slug_index < 0) return;
  unsigned int i;
  std::string plural = slug;
  plural.append("s");
  if (!hasParameter(&clipboard, slug)) {
    titleWindow->clear();
    titleWindow->addLine(Window::BOLD + Window::RED + "Clipboard has no " + slug + Window::RESET);
    return;
  }
  std::string name;
  if (classes[working_directory][active_json][plural][active_slug_index]["name"] == "_")
    name = clipboard[slug]["name"];
  else
    name = classes[working_directory][active_json][plural][active_slug_index]["name"];
  classes[working_directory][active_json][plural][active_slug_index] = clipboard[slug];
  classes[working_directory][active_json][plural][active_slug_index]["name"] = name;
  checkDuplicatedSlugName(plural);
  name = classes[working_directory][active_json][plural][active_slug_index]["name"];
  nlohmann::json j = classes[working_directory][active_json][plural][active_slug_index];
  classes[working_directory][active_json][plural].erase(active_slug_index);
  for (i = 0; i < classes[working_directory][active_json][plural].size(); ++i)
    if (j["ord"] < classes[working_directory][active_json][plural][i]["ord"]) break;
  classes[working_directory][active_json][plural].insert(classes[working_directory][active_json][plural].begin() + i, j);
  for (int i = 0; i < classes[working_directory][active_json][plural].size(); ++i)
    if (classes[working_directory][active_json][plural][i]["name"] == name) {
      active_slug_index = i;
      break;
    }
  showActiveJSON();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Pasted " + slug + " from clipboard" + Window::RESET);
  changes_made = true;
}

void JsonCreator::callbackRemoveSlug(std::string slug)
{
  if (active_json.empty() || active_slug_index < 0) return;
  classes[working_directory][active_json][slug].erase(active_slug_index);
  if (classes[working_directory][active_json][slug].empty())
    classes[working_directory][active_json].erase(slug);
  active_slug_index = -1;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveSlugMultiple(std::string slug)
{
  if (active_slug.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()]))
      for (int i = 0; i < classes[x.key()][y.key()][slug].size(); ++i)
        if (classes[x.key()][y.key()][slug][i]["name"] == active_slug) {
          classes[x.key()][y.key()][slug].erase(i);
          if (classes[x.key()][y.key()][slug].empty())
            classes[x.key()][y.key()].erase(slug);
          break;
        }
  active_slug.clear();
  changes_made = true;
}

void JsonCreator::callbackSelectTrigger()
{
  if (argument.empty() || active_json.empty()) return;
  int i;
  bool found = false;
  active_trigger = argument;
  for (i = 0; i < classes[working_directory][active_json]["triggers"].size(); ++i)
    if (classes[working_directory][active_json]["triggers"][i]["#a#label"] == argument) {
      found = true;
      break;
    }
  active_trigger_index = i;
  if (!found) {
    nlohmann::json j = defaults["triggers"];
    j["#a#label"] = argument;
    classes[working_directory][active_json]["triggers"].push_back(j);
    changes_made = true;
  }
  completions["trigger_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackSelectTriggerMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  int i;
  bool found = false;
  active_trigger = argument;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          found = true;
          break;
        }
      if (!found) {
        nlohmann::json j = defaults["triggers"];
        j["#a#label"] = active_trigger;
        classes[x.key()][y.key()]["triggers"].push_back(j);
        changes_made = true;
      }
    }
  completions["trigger_labels"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackEditTriggerLabel()
{
  if (argument.empty() || active_json.empty() || active_trigger_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"] = argument;
  checkDuplicatedTriggerLabel();
  argument = classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"];
  completions["trigger_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetTriggerField(std::string field_name, std::string completion_name)
{
  if (argument.empty() || active_json.empty() || active_trigger_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index][field_name] = argument;
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetTriggerFieldMultiple(std::string field_name, std::string completion_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          classes[x.key()][y.key()]["triggers"][i][field_name] = argument;
          break;
        }
    }
  completions[completion_name][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveTriggerType()
{
  if (active_json.empty()) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index].erase("type");
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveTriggerTypeMultiple()
{
  if (filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          classes[x.key()][y.key()]["triggers"][i].erase("type");
          break;
        }
    }
  changes_made = true;
}

void JsonCreator::callbackAddTriggerTag()
{
  if (argument.empty() || active_json.empty() || active_trigger_index < 0) return;
  bool found = false;
  completions["tag_names"][argument] = true;
  for (int i = 0; i < classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"].size() && !found; ++i)
    if (classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"][i] == argument)
      found = true;
  if (!found) classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"].push_back(argument);
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackAddTriggerTagMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  bool found;
  completions["tag_names"][argument] = true;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          found = false;
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["tags"].size() && !found; ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["tags"][j] == argument) {
              found = true;
              break;
            }
          if (!found) classes[x.key()][y.key()]["triggers"][i]["tags"].push_back(argument);
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveTriggerTag()
{
  if (argument.empty() || active_json.empty() || active_trigger_index < 0) return;
  completions["tag_names"][argument] = true;
  for (int i = 0; i < classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"].size(); ++i)
    if (classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"][i] == argument) {
      classes[working_directory][active_json]["triggers"][active_trigger_index]["tags"].erase(i);
      break;
    }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveTriggerTagMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  completions["tag_names"][argument] = true;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["tags"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["tags"][j] == argument) {
              classes[x.key()][y.key()]["triggers"][i]["tags"].erase(j);
              break;
            }
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackMoveTriggerUp()
{
  if (active_json.empty() || active_trigger_index <= 0) return;
  nlohmann::json c = classes[working_directory][active_json]["triggers"][active_trigger_index-1];
  classes[working_directory][active_json]["triggers"][active_trigger_index-1] = classes[working_directory][active_json]["triggers"][active_trigger_index];
  classes[working_directory][active_json]["triggers"][active_trigger_index] = c;
  --active_trigger_index;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackMoveTriggerDown()
{
  if (active_json.empty() || active_trigger_index < 0 ||
    active_trigger_index >= classes[working_directory][active_json]["triggers"].size() - 1) return;
  nlohmann::json c = classes[working_directory][active_json]["triggers"][active_trigger_index+1];
  classes[working_directory][active_json]["triggers"][active_trigger_index+1] = classes[working_directory][active_json]["triggers"][active_trigger_index];
  classes[working_directory][active_json]["triggers"][active_trigger_index] = c;
  ++active_trigger_index;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackCopyTrigger()
{
  if (active_json.empty() || active_trigger_index < 0) return;
  clipboard["trigger"] = classes[working_directory][active_json]["triggers"][active_trigger_index];
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Copied trigger to clipboard" + Window::RESET);
}

void JsonCreator::callbackPasteTrigger()
{
  if (active_json.empty() || active_trigger_index < 0) return;
  if (!hasParameter(&clipboard, "trigger")) {
    titleWindow->clear();
    titleWindow->addLine(Window::BOLD + Window::RED + "Clipboard has no trigger" + Window::RESET);
    return;
  }
  std::string name;
  if (classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"] == "_")
    name = clipboard["trigger"]["#a#label"];
  else
    name = classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"];
  classes[working_directory][active_json]["triggers"][active_trigger_index] = clipboard["trigger"];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["#a#label"] = name;
  checkDuplicatedTriggerLabel();
  showActiveJSON();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Pasted trigger from clipboard" + Window::RESET);
  changes_made = true;
}

void JsonCreator::callbackRemoveTrigger()
{
  if (active_json.empty() || active_trigger_index < 0) return;
  classes[working_directory][active_json]["triggers"].erase(active_trigger_index);
  if (classes[working_directory][active_json]["triggers"].empty())
    classes[working_directory][active_json].erase("triggers");
  active_trigger_index = -1;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveTriggerMultiple()
{
  if (active_trigger.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          classes[x.key()][y.key()]["triggers"].erase(i);
          if (classes[x.key()][y.key()]["triggers"].empty())
            classes[x.key()][y.key()].erase("triggers");
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSelectCondition()
{
  if (argument.empty() || active_json.empty()) return;
  int i;
  bool found = false;
  active_condition = argument;
  for (i = 0; i < classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].size(); ++i)
    if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][i]["#a#label"] == argument) {
      found = true;
      break;
    }
  active_condition_index = i;
  if (!found) {
    nlohmann::json j;
    j["#a#label"] = argument;
    classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].push_back(j);
    changes_made = true;
  }
  completions["condition_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackSelectConditionMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  int i;
  bool found;
  active_condition = argument;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          found = false;
          for (int k = 0; k < classes[x.key()][y.key()]["triggers"][i]["conditions"].size() && !found; ++k)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][k]["#a#label"] == argument)
              found = true;
          if (!found) {
            nlohmann::json j;
            j["#a#label"] = argument;
            classes[x.key()][y.key()]["triggers"][i]["conditions"].push_back(j);
            changes_made = true;
          }
          break;
        }
    }
  completions["condition_labels"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackEditConditionLabel()
{
  if (argument.empty() || active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"] = argument;
  checkDuplicatedConditionLabel();
  argument = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"];
  completions["condition_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetConditionField(std::string field_name, std::string completion_name)
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index][field_name] = argument;
  completions[completion_name][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetConditionFieldMultiple(std::string field_name, std::string completion_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              classes[x.key()][y.key()]["triggers"][i]["conditions"][j][field_name] = argument;
              break;
            }
            break;
        }
    }
    completions[completion_name][argument] = true;
    argument.clear();
    changes_made = true;
}

void JsonCreator::callbackRemoveConditionField(std::string field_name)
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index].erase(field_name);
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveConditionFieldMultiple(std::string field_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              classes[x.key()][y.key()]["triggers"][i]["conditions"][j].erase(field_name);
              break;
            }
            break;
        }
    }
    changes_made = true;
}

void JsonCreator::callbackSetConditionValue(std::string field_name)
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  try {
    classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index][field_name] = std::stod(argument);
  } catch (...) {
    classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index].erase(field_name);
  }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetConditionValueMultiple(std::string field_name)
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              try {
                classes[x.key()][y.key()]["triggers"][i]["conditions"][j][field_name] = std::stod(argument);
              } catch (...) {
                classes[x.key()][y.key()]["triggers"][i]["conditions"][j].erase(field_name);
              }
              break;
            }
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackAddConditionTag()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  bool found = false;
  for (int i = 0; i < classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"].size() && !found; ++i)
    if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"][i] == argument)
      found = true;
  if (!found) classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"].push_back(argument);
  completions["tag_names"][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackAddConditionTagMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  bool found;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              found = false;
              for (int k = 0; k < classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"].size() && !found; ++k)
                if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"][k] == argument)
                  found = true;
              if (!found) classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"].push_back(argument);
              break;
            }
          break;
        }
    }
  completions["tag_names"][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveConditionTag()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  for (int i = 0; i < classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"].size(); ++i)
    if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"][i] == argument) {
      classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"].erase(i);
      if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["tags"].empty())
        classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index].erase("tags");
      break;
    }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveConditionTagMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              for (int k = 0; k < classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"].size(); ++k)
                if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"][k] == argument) {
                  classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"].erase(k);
                  if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["tags"].empty())
                    classes[x.key()][y.key()]["triggers"][i]["conditions"][j].erase("tags");
                  break;
                }
              break;
            }
          break;
        }
    }
  completions["tag_names"][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackMoveConditionUp()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index <= 0) return;
  nlohmann::json c = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index-1];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index-1] =
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index] = c;
  --active_condition_index;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackMoveConditionDown()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0 ||
    active_condition_index >= classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].size() - 1) return;
  nlohmann::json c = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index+1];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index+1] =
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index] = c;
  ++active_condition_index;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackCopyCondition()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  clipboard["condition"] = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index];
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Copied condition to clipboard" + Window::RESET);
}

void JsonCreator::callbackPasteCondition()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  if (!hasParameter(&clipboard, "condition")) {
    titleWindow->clear();
    titleWindow->addLine(Window::BOLD + Window::RED + "Clipboard has no condition" + Window::RESET);
    return;
  }
  std::string name;
  if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"] == "_")
    name = clipboard["condition"]["#a#label"];
  else
    name = classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index] = clipboard["condition"];
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"][active_condition_index]["#a#label"] = name;
  checkDuplicatedConditionLabel();
  showActiveJSON();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Pasted condition from clipboard" + Window::RESET);
  changes_made = true;
}

void JsonCreator::callbackRemoveCondition()
{
  if (active_json.empty() || active_trigger_index < 0 || active_condition_index < 0) return;
  classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].erase(active_condition_index);
  if (classes[working_directory][active_json]["triggers"][active_trigger_index]["conditions"].empty())
    classes[working_directory][active_json]["triggers"][active_trigger_index].erase("conditions");
  active_condition_index = -1;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveConditionMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["triggers"].size(); ++i)
        if (classes[x.key()][y.key()]["triggers"][i]["#a#label"] == active_trigger) {
          for (int j = 0; j < classes[x.key()][y.key()]["triggers"][i]["conditions"].size(); ++j)
            if (classes[x.key()][y.key()]["triggers"][i]["conditions"][j]["#a#label"] == active_condition) {
              classes[x.key()][y.key()]["triggers"][i]["conditions"].erase(j);
              if (classes[x.key()][y.key()]["triggers"][i]["conditions"].empty())
                classes[x.key()][y.key()]["triggers"][i].erase("conditions");
              break;
            }
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackSelectBiomeInit()
{
  if (argument.empty() || active_json.empty()) return;
  int i;
  bool found = false;
  active_biome_init = argument;
  for (i = 0; i < classes[working_directory][active_json]["init"].size(); ++i)
    if (classes[working_directory][active_json]["init"][i]["label"] == argument) {
      found = true;
      break;
    }
  active_biome_init_index = i;
  if (!found) {
    nlohmann::json j;
    j["label"] = argument;
    j["attributes"] = nlohmann::json::parse("{}");
    classes[working_directory][active_json]["init"].push_back(j);
    changes_made = true;
  }
  completions["init_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackSelectBiomeInitMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  bool found;
  active_biome_init = argument;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      found = false;
      for (int i = 0; i < classes[x.key()][y.key()]["init"].size() && !found; ++i)
        if (classes[x.key()][y.key()]["init"][i]["label"] == argument)
          found = true;
      if (!found) {
        nlohmann::json j;
        j["label"] = argument;
        j["attributes"] = nlohmann::json::parse("{}");
        classes[x.key()][y.key()]["init"].push_back(j);
        changes_made = true;
      }
    }
  completions["init_labels"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackEditInitLabel()
{
  if (argument.empty() || active_json.empty() || active_biome_init_index < 0) return;
  classes[working_directory][active_json]["init"][active_biome_init_index]["label"] = argument;
  checkDuplicatedInitLabel();
  argument = classes[working_directory][active_json]["init"][active_biome_init_index]["label"];
  completions["init_labels"][argument] = true;
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSelectBiomeInitAttribute()
{
  if (argument.empty() || active_json.empty() || active_biome_init_index < 0) return;
  active_attribute = argument;
  if (!hasParameter(&classes[working_directory][active_json]["init"][active_biome_init_index]["attributes"], argument)) {
    classes[working_directory][active_json]["init"][active_biome_init_index]["attributes"][argument] = nlohmann::json::parse("{}");
    changes_made = true;
  }
  completions["attributes"][argument] = true;
  argument.clear();
  showActiveJSON();
}

void JsonCreator::callbackSelectBiomeInitAttributeMultiple()
{
  if (argument.empty() || active_biome_init.empty() || filtered_list.empty()) return;
  active_attribute = argument;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["init"].size(); ++i)
        if (classes[x.key()][y.key()]["init"][i]["label"] == active_biome_init) {
          if (!hasParameter(&classes[x.key()][y.key()]["init"][i]["attributes"], argument)) {
            classes[x.key()][y.key()]["init"][i]["attributes"][argument] = nlohmann::json::parse("{}");
            changes_made = true;
          }
          break;
        }
    }
  completions["attributes"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackSetBiomeInitAttribute(std::string field_name)
{
  if (argument.empty() || active_json.empty() || active_biome_init_index < 0 || active_attribute.empty()) return;
  try {
    classes[working_directory][active_json]["init"][active_biome_init_index]["attributes"][active_attribute][field_name] = (double) std::stod(argument);
  } catch (...) {
    classes[working_directory][active_json]["init"][active_biome_init_index]["attributes"][active_attribute].erase(field_name);
  }
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackSetBiomeInitAttributeMultiple(std::string field_name)
{
  if (argument.empty() || active_biome_init.empty() || active_attribute.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["init"].size(); ++i)
        if (classes[x.key()][y.key()]["init"][i]["label"] == active_biome_init) {
          try {
            classes[x.key()][y.key()]["init"][i]["attributes"][active_attribute][field_name] = (double) std::stod(argument);
          } catch (...) {
            classes[x.key()][y.key()]["init"][i]["attributes"][active_attribute].erase(field_name);
          }
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveBiomeInitAttribute()
{
  if (active_json.empty() || active_biome_init_index < 0 || active_attribute.empty()) return;
  classes[working_directory][active_json]["init"][active_biome_init_index]["attributes"].erase(active_attribute);
  argument.clear();
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveBiomeInitAttributeMultiple()
{
  if (active_biome_init.empty() || active_attribute.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["init"].size(); ++i)
        if (classes[x.key()][y.key()]["init"][i]["label"] == active_biome_init) {
          classes[x.key()][y.key()]["init"][i]["attributes"].erase(active_attribute);
          break;
        }
    }
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackCopyBiomeInit()
{
  if (active_json.empty() || active_biome_init_index < 0) return;
  clipboard["init"] = classes[working_directory][active_json]["init"][active_biome_init_index];
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Copied init to clipboard" + Window::RESET);
}

void JsonCreator::callbackPasteBiomeInit()
{
  if (active_json.empty() || active_biome_init_index < 0) return;
  if (!hasParameter(&clipboard, "init")) {
    titleWindow->clear();
    titleWindow->addLine(Window::BOLD + Window::RED + "Clipboard has no init" + Window::RESET);
    return;
  }
  std::string name;
  if (classes[working_directory][active_json]["init"][active_biome_init_index]["label"] == "_")
    name = clipboard["init"]["label"];
  else
    name = classes[working_directory][active_json]["init"][active_biome_init_index]["label"];
  classes[working_directory][active_json]["init"][active_biome_init_index] = clipboard["init"];
  classes[working_directory][active_json]["init"][active_biome_init_index]["label"] = name;
  checkDuplicatedInitLabel();
  showActiveJSON();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + Window::GREEN + "Pasted init from clipboard" + Window::RESET);
  changes_made = true;
}

void JsonCreator::callbackRemoveBiomeInit()
{
  if (active_json.empty() || active_biome_init_index < 0) return;
  classes[working_directory][active_json]["init"].erase(active_biome_init_index);
  if (classes[working_directory][active_json]["init"].empty())
    classes[working_directory][active_json].erase("init");
  active_biome_init_index = -1;
  showActiveJSON();
  changes_made = true;
}

void JsonCreator::callbackRemoveBiomeInitMultiple()
{
  if (active_biome_init.empty() || active_attribute.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["init"].size(); ++i)
        if (classes[x.key()][y.key()]["init"][i]["label"] == active_biome_init) {
          classes[x.key()][y.key()]["init"].erase(i);
          if (classes[x.key()][y.key()]["init"].empty())
            classes[x.key()][y.key()].erase("init");
          break;
        }
    }
  argument.clear();
  active_biome_init.clear();
  changes_made = true;
}

void JsonCreator::callbackAddEcoregion()
{
  if (active_json.empty() || argument.empty()) return;
  bool found = false;
  if (!hasParameter(&classes[working_directory][active_json], "ecoregions"))
    classes[working_directory][active_json]["ecoregions"] = nlohmann::json::parse("[]");
  for (int i = 0; i < classes[working_directory][active_json]["ecoregions"].size() && !found; ++i)
    if (classes[working_directory][active_json]["ecoregions"][i] == argument)
      found = true;
  if (!found) {
    classes[working_directory][active_json]["ecoregions"].push_back(argument);
    changes_made = true;
  }
  completions["ecoregions"][argument] = true;
  showActiveJSON();
}

void JsonCreator::callbackAddEcoregionMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  bool found;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      found = false;
      if (!hasParameter(&classes[x.key()][y.key()], "ecoregions"))
        classes[x.key()][y.key()]["ecoregions"] = nlohmann::json::parse("[]");
      for (int i = 0; i < classes[x.key()][y.key()]["ecoregions"].size() && !found; ++i)
        if (classes[x.key()][y.key()]["ecoregions"][i] == argument)
          found = true;
      if (!found) classes[x.key()][y.key()]["ecoregions"].push_back(argument);
    }
  completions["ecoregions"][argument] = true;
  argument.clear();
  changes_made = true;
}

void JsonCreator::callbackRemoveEcoregion()
{
  if (active_json.empty() || argument.empty()) return;
  for (int i = 0; i < classes[working_directory][active_json]["ecoregions"].size(); ++i)
    if (classes[working_directory][active_json]["ecoregions"][i] == argument) {
      classes[working_directory][active_json]["ecoregions"].erase(i);
      if (classes[working_directory][active_json]["ecoregions"].empty())
        classes[working_directory][active_json].erase("ecoregions");
      changes_made = true;
      break;
    }
  completions["ecoregions"][argument] = true;
  showActiveJSON();
}

void JsonCreator::callbackRemoveEcoregionMultiple()
{
  if (argument.empty() || filtered_list.empty()) return;
  for (auto& x : nlohmann::json::iterator_wrapper(filtered_list))
    for (auto& y : nlohmann::json::iterator_wrapper(filtered_list[x.key()])) {
      for (int i = 0; i < classes[x.key()][y.key()]["ecoregions"].size(); ++i)
        if (classes[x.key()][y.key()]["ecoregions"][i] == argument) {
          classes[x.key()][y.key()]["ecoregions"].erase(i);
          if (classes[x.key()][y.key()]["ecoregions"].empty())
            classes[x.key()][y.key()].erase("ecoregions");
          changes_made = true;
          break;
        }
    }
  completions["ecoregions"][argument] = true;
  argument.clear();
}

void JsonCreator::callbackCheckJSONsIntegrity()
{
  previewWindow->clear();
  titleWindow->clear();
  titleWindow->addLine(Window::BOLD + "Last integrity check result" + Window::RESET);
  for (int i = 0; i < checks.size(); ++i) previewWindow->addLine(checks[i]);
}

void JsonCreator::callbackRunIntegrityCheck()
{
  previewWindow->clear();
  runIntegrityCheks();
  for (int i = 0; i < checks.size(); ++i) previewWindow->addLine(checks[i]);
}

void JsonCreator::callbackExportAll()
{
  std::string old_pwd = working_directory;
  std::string dir, file_path;
  for (auto& x : nlohmann::json::iterator_wrapper(deletions))
    for (auto& y : nlohmann::json::iterator_wrapper(deletions[x.key()])) {
      file_path = std::string("../") + x.key() + "/" + y.key() + ".json";
      remove(file_path.c_str());
    }
  deletions.clear();
  for (auto& x : nlohmann::json::iterator_wrapper(classes)) {
    dir = std::string("../") + x.key();
    chdir(dir.c_str());
    working_directory = x.key();
    exportClasses();
  }
  dir = std::string("../") + old_pwd;
  chdir(dir.c_str());
  working_directory = old_pwd;
  exportIndex();
  changes_made = false;
}
