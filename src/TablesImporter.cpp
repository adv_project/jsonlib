/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/TablesImporter.h"

TablesImporter::TablesImporter(std::string dboptions, std::string gamename)
{
  this->dboptions = dboptions;
  this->gamename = gamename;
}

// Read all index.json files and import those tables into the database
void TablesImporter::importTables()
{
  nlohmann::json j, index, attr, attributes, directories;
  std::ifstream file;
  ntables = 0;
  nboundaries = 0;
  unsigned long nattributes = 0;
  std::string pgsqlcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
  attributes_query = std::string("set search_path to ") + gamename + ";\nbegin;\n";
  // Read properties file
  file.open("properties.json");
  if (file.good() && file.peek() != std::ifstream::traits_type::eof()) {
    try { file >> j; } catch (...) {
      PG_log("Invalid properties JSON!", "error");
      file.close();
      return;
    }
    file.close();
  }
  else {
    PG_log("Couldn't read properties file!", "error");
    return;
  }
  if ( !hasParameter(&j, "mods") || (j["mods"].type() != nlohmann::json::value_t::array) ||
    j["mods"].size() == 0 || (j["mods"][0].type() != nlohmann::json::value_t::string) ) {
    PG_log("Couldn't read mods from properties file!", "error");
    return;
  }
  // Read all index files and merge them into one
  for (int i = 0; i < j["mods"].size(); ++i) {
    file.open(std::string(std::string("mods/") + j["mods"][i].get<std::string>() + "/json/index.json").c_str());
    if (file.good() && file.peek() != std::ifstream::traits_type::eof()) {
      file.clear();
      try { file >> index; } catch (...) {
        PG_log("Invalid index JSON in mod " + j["mods"][i].get<std::string>(), "error");
        file.close();
        continue;
      }
      file.close();
    }
    else {
      PG_log("Mod " + j["mods"][i].get<std::string>() + " doesn't appear to have an index file.", "info");
      continue;
    }
    PG_log("Importing tables from mod " + j["mods"][i].get<std::string>());
    if ( hasParameter(&index, "directories") && (index["directories"].type() == nlohmann::json::value_t::array) &&
      index["directories"].size() > 0 && (index["directories"][0].type() == nlohmann::json::value_t::string) )
      for (int i = 0; i < index["directories"].size(); ++i) directories[index["directories"][i].get<std::string>()] = true;
    if ( !hasParameter(&index, "attributes") || (index["attributes"].type() != nlohmann::json::value_t::object) )
      continue;
    for (auto& x : nlohmann::json::iterator_wrapper(index["attributes"])) {
      if ( !hasParameter(&x.value(), "table") || (x.value()["table"].type() != nlohmann::json::value_t::string) || x.value()["table"].get<std::string>().empty() ) {
        PG_log(std::string("Attribute ") + x.key() + " has no assigned table. Skipping.", "warning");
        continue;
      }
      if ( !hasParameter(&x.value(), "type") || (x.value()["type"].type() != nlohmann::json::value_t::string) ) {
        PG_log(std::string("Attribute ") + x.key() + " has no type. Skipping.", "warning");
        continue;
      }
      if ( (x.value()["type"] != "boolean") && (x.value()["type"] != "integer") && (x.value()["type"] != "numeric") ) {
        PG_log(std::string("Attribute ") + x.key() + " has no valid type (" + x.value()["type"].get<std::string>() + "). Skipping.", "warning");
        continue;
      }
      attr["type"] = x.value()["type"];
      attr["table"] = x.value()["table"];
      if ( hasParameter(&attributes, x.key()) && hasParameter(&attributes[x.key()], "boundaries") )
        attr["boundaries"] = attributes[x.key()]["boundaries"];
      if ( hasParameter(&x.value(), "boundaries") && (x.value()["boundaries"].type() == nlohmann::json::value_t::object) )
        for (auto& y : nlohmann::json::iterator_wrapper(x.value()["boundaries"]))
          if (TablesImporter::isNumber(y.value())) attr["boundaries"][y.key()] = y.value();
      attributes[x.key()] = attr;
      attr.clear();
    }
    index.clear();
  }
  j.clear();
  // Parameterize the attributes list by table
  for (auto& x : nlohmann::json::iterator_wrapper(attributes)) {
    attr.clear();
    attr["name"] = x.key();
    attr["type"] = x.value()["type"];
    if (hasParameter(&x.value(), "boundaries")) attr["boundaries"] = x.value()["boundaries"];
    j[x.value()["table"].get<std::string>()].push_back(attr);
    ++nattributes;
  }
  pgsqlcmd += "create table directories (\n  name varchar primary key\n);\n"; // Create directories table
  for (auto& x : nlohmann::json::iterator_wrapper(directories)) {
    if (!hasParameter(&j, x.key())) j[x.key()] = nlohmann::json::parse("[]"); // Include table with no attributes
    pgsqlcmd += PG_buildInsert("directories", "(name)", std::string("('") + x.key() + "')", true); // Add table name to the directories table
  }
  // Finally generate the table creation queries
  for (auto& x : nlohmann::json::iterator_wrapper(j))
    pgsqlcmd += PG_createTable(x.key(), x.value(), hasParameter(&directories, x.key()));
  attributes_query += "commit;\n";
  pgsqlcmd += "commit;\n";
  PGconn* conn = PQconnectdb(dboptions.c_str());
  PGresult* res = PQexec(conn, attributes_query.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK || PQresultStatus(res) == PGRES_COMMAND_OK)
    PQclear(res);
  else {
    PG_log(std::string("Attributes commit error: ") + PQresultErrorMessage(res), "error");
  }
  PQfinish(conn);
  conn = PQconnectdb(dboptions.c_str());
  res = PQexec(conn, pgsqlcmd.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK || PQresultStatus(res) == PGRES_COMMAND_OK)
    PQclear(res);
  else {
    PG_log(std::string("Tables commit error: ") + PQresultErrorMessage(res), "error");
  }
  PQfinish(conn);
  std::string log_message = "Imported ${{ntables}} tables having ${{nattributes}} attributes and ${{nboundaries}} boundaries.";
  log_message = replaceAllInstances(log_message, "${{ntables}}", std::to_string(ntables));
  log_message = replaceAllInstances(log_message, "${{nattributes}}", std::to_string(nattributes));
  log_message = replaceAllInstances(log_message, "${{nboundaries}}", std::to_string(nboundaries));
  PG_log(log_message);
  return;
}

std::string TablesImporter::PG_addAnything(std::string id, std::string type, std::string in_table)
{
  std::string pgsqlcmd = PG_buildInsert("everything", "(id)", "('${{id}}')", true);
  pgsqlcmd += PG_buildUpdate("everything", "type='${{type}}',in_table='${{in_table}}'", "where id='${{id}}'");
  if (type != "attribute") pgsqlcmd += PG_buildInsert("${{in_table}}", "(id)", "('${{id}}')", true);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{type}}", type);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{in_table}}", in_table);
  return pgsqlcmd;
}

std::string TablesImporter::PG_createTable(std::string name, nlohmann::json &attributes, bool is_directory)
{
  std::string boundary, boundaries;
  std::string pgsqlcmd = std::string("create table ") + name + " (\n  id varchar primary key references everything on delete cascade";
  if (is_directory) pgsqlcmd += ",\n  classes varchar[],\n  tags varchar[]";
  for (int i = 0; i < attributes.size(); ++i) {
    attributes_query += PG_addAnything(attributes[i]["name"].get<std::string>(), "attribute", name); // Import attribute
    pgsqlcmd += ",\n  " + attributes[i]["name"].get<std::string>() + " " + attributes[i]["type"].get<std::string>();
    if (hasParameter(&attributes[i], "boundaries"))
      for (auto& x : nlohmann::json::iterator_wrapper(attributes[i]["boundaries"])) { // Create all boundaries
        boundary = PG_buildInsert("boundaries", "(id, label, value)", "('${{id}}', '${{label}}', ${{value}})", false);
        boundary = replaceAllInstances(boundary, "${{id}}", attributes[i]["name"]);
        boundary = replaceAllInstances(boundary, "${{label}}", x.key());
        boundary = replaceAllInstances(boundary, "${{value}}", std::to_string((double) x.value()));
        boundaries += boundary;
        ++nboundaries;
      }
  }
  pgsqlcmd += "\n);\n";
  pgsqlcmd += boundaries;
  ++ntables;
  return pgsqlcmd;
}

void TablesImporter::PG_log(std::string log_message, std::string log_level)
{
  PGconn *conn = PQconnectdb(dboptions.c_str());
  std::string log_pgsqlcmd = std::string("select log_event('") + log_level + "', 'tables_importer', '" + log_message + "');\n";
  log_pgsqlcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n" + log_pgsqlcmd + "commit;\n";
  PGresult *res = PQexec(conn, log_pgsqlcmd.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) PQclear(res);
  PQfinish(conn);
}

std::string TablesImporter::PG_buildInsert(std::string table, std::string keys, std::string values, bool conflicts)
{
  std::string insert = std::string("insert into ") + table + " " + keys + " values " + values;
  if (conflicts) insert += "\n  on conflict do nothing";
  insert += ";\n";
  return insert;
}

std::string TablesImporter::PG_buildUpdate(std::string table, std::string values, std::string where)
{
  if (values.empty()) return "";
  return std::string("update ") + table + " set " + values + "\n  " + where + ";\n";
}

bool TablesImporter::isNumber(nlohmann::json &j)
{
  if (j.type() == nlohmann::json::value_t::number_unsigned ||
    j.type() == nlohmann::json::value_t::number_integer ||
    j.type() == nlohmann::json::value_t::number_float)
    return true;
  else
    return false;
}

std::string TablesImporter::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

std::string TablesImporter::replaceAllBoolInstances(std::string string, std::string grab, bool replace_boolean)
{
  int n = 0;
  std::string replace;
  if (replace_boolean) replace = "true"; else replace = "false";
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}
