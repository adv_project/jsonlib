/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/ClassesImporter.h"

ClassesImporter::ClassesImporter(std::string dboptions, std::string gamename)
{
  nlohmann::json attr;
  this->dboptions = dboptions;
  this->gamename = gamename;
  ask_id_query = std::string("set search_path to ") + gamename + "; select * from make();";
  std::string attrs_query = std::string("set search_path to ") + gamename + ";\n";
  attrs_query += "select distinct(e.id), i.data_type, e.in_table from\n";
  attrs_query += "everything e inner join information_schema.columns i\n";
  attrs_query += "on i.column_name = e.id and e.type = 'attribute';";
  PGconn* conn = PQconnectdb(dboptions.c_str());
  PGresult* res = PQexec(conn, attrs_query.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) {
    int nrows = PQntuples(res);
    for(int i = 0; i < nrows; ++i) {
      attr.clear();
      attr["type"] = PQgetvalue(res, i, 1);
      attr["table"] = PQgetvalue(res, i, 2);
      attributes[PQgetvalue(res, i, 0)] = attr;
    }
    PQclear(res);
  }
  PQfinish(conn);
}

// Read all classes found at the current working directory and import them into the database
// Returns the number of imported classes
void ClassesImporter::importClasses()
{
  nlohmann::json j;
  all_tags.clear();
  std::ifstream json_file;
  nclasses = 0;
  nobjects = 0;
  PGconn* conn = PQconnectdb(dboptions.c_str());
  PGresult* res;
  std::string pgsqlcmd, log_message;
  DIR *dir;
  struct dirent *entry;
  std::list<std::string> entries_list;
  // Read every directory for class JSON files
  if ((dir = opendir(".")) != NULL) {
    while ((entry = readdir(dir)) != NULL)
      if ( strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..") )
        entries_list.push_back(entry->d_name);
    closedir(dir);
  }
  for (auto& it: entries_list) {
    if ((dir = opendir(it.c_str())) != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") ) continue;
        if (entry->d_name[0] == '_') {
          log_message = std::string("Ignoring template class ") + it + "/" + entry->d_name;
          PG_log(log_message, "info");
          continue;
        }
        json_file.open(std::string(it + "/" + entry->d_name).c_str());
        if (json_file.good() && json_file.peek() != std::ifstream::traits_type::eof()) {
          j.clear();
          try { json_file >> j; } catch (...) {
            log_message = std::string("Invalid JSON ") + it + "/" + entry->d_name;
            PG_log(log_message, "error");
            continue;
          }
          json_file.close();
          // Parse JSON file
          if ( hasParameter(&j, "init") ) j.erase("init");
          if ( !hasParameter(&j, "id") || (j["id"].type() != nlohmann::json::value_t::string) ) {
            log_message = std::string("Class file ") + it + "/" + entry->d_name + " has no valid id field. Skipping.";
            PG_log(log_message, "warning");
            continue;
          }
          if ( !hasParameter(&j, "type") || (j["type"].type() != nlohmann::json::value_t::string) ) {
            log_message = std::string("Class file ") + it + "/" + entry->d_name + " has no valid type field. Skipping.";
            PG_log(log_message, "warning");
            continue;
          }
          pgsqlcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
          pgsqlcmd += PG_addAnything(j["id"], j["type"], it);
          for (auto& x : nlohmann::json::iterator_wrapper(j)) {
            if (x.key() == "classes") pgsqlcmd += PG_addClasses(j["id"], it, x.value());
            else if (x.key() == "tags") pgsqlcmd += PG_addTags(j["id"], it, x.value());
            else if (x.key() == "attributes") pgsqlcmd += PG_addAttributes(j["id"], x.value());
            else if (x.key() == "plugs") pgsqlcmd += PG_addPlugs(j["id"], x.value());
            else if (x.key() == "slots") pgsqlcmd += PG_addSlots(j["id"], x.value());
            else if (x.key() == "slices") pgsqlcmd += PG_addSlices(j["id"], x.value());
            else if (x.key() == "triggers") pgsqlcmd += PG_addTriggers(j["id"], x.value());
          }
          pgsqlcmd += "commit;\n";
          res = PQexec(conn, pgsqlcmd.c_str());
          if (PQresultStatus(res) == PGRES_TUPLES_OK || PQresultStatus(res) == PGRES_COMMAND_OK)
            PQclear(res);
          else {
            PG_log(std::string("Commit error: ") + PQresultErrorMessage(res), "error");
          }
        }
      }
      closedir(dir);
    }
  }
  log_message = "Imported ${{nclasses}} classes having ${{ntags}} tags.";
  unsigned long ntags = 0;
  for (auto& x : nlohmann::json::iterator_wrapper(all_tags)) ++ntags;
  log_message = replaceAllInstances(log_message, "${{nclasses}}", std::to_string(nclasses));
  log_message = replaceAllInstances(log_message, "${{ntags}}", std::to_string(ntags));
  PG_log(log_message);
  return;
}

std::string ClassesImporter::PG_addAnything(std::string id, std::string type, std::string in_table)
{
  std::string pgsqlcmd = PG_buildInsert("everything", "(id)", "('${{id}}')", true);
  pgsqlcmd += PG_buildUpdate("everything", "type='${{type}}',in_table='${{in_table}}'", "where id='${{id}}'");
  if (type != "attribute") pgsqlcmd += PG_buildInsert("${{in_table}}", "(id)", "('${{id}}')", true);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{type}}", type);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{in_table}}", in_table);
  ++nclasses;
  if (in_table == "classes") ++nobjects;
  return pgsqlcmd;
}

std::string ClassesImporter::PG_addClasses(std::string id, std::string table, nlohmann::json &classes)
{
  std::string pgsqlcmd = PG_buildInsert(table, "(id)", "('${{id}}')", true);
  pgsqlcmd += PG_buildUpdate(table, std::string("classes=array(select distinct(unnest(classes || ${{classes}})) as classes from ")
    + table + " where id='${{id}}')", "where id='${{id}}'");
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{classes}}", PG_buildArray(classes, false));
  return pgsqlcmd;
}

std::string ClassesImporter::PG_addTags(std::string id, std::string table, nlohmann::json &tags)
{
  std::string pgsqlcmd = PG_buildInsert(table, "(id)", "('${{id}}')", true);
  pgsqlcmd += PG_buildUpdate(table, std::string("tags=array(select distinct(unnest(tags || ${{tags}})) as tags from ")
    + table + " where id='${{id}}')", "where id='${{id}}'");
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{tags}}", PG_buildArray(tags, true));
  return pgsqlcmd;
}

std::string ClassesImporter::PG_addAttributes(std::string id, nlohmann::json &attributes)
{
  std::string pgsqlfullcmd, pgsqlcmd, type, table, value, log_message;
  for (auto& x : nlohmann::json::iterator_wrapper(attributes)) {
    type = getAttributeType(x.key());
    table = getAttributeTable(x.key());
    if (type.empty() || table.empty()) continue;
    pgsqlcmd = PG_buildInsert("${{table}}", "(id)", "('${{id}}')", true);
    pgsqlcmd += PG_buildUpdate("${{table}}", "${{name}}=${{value}}", "where id='${{id}}'");
    pgsqlcmd += PG_buildInsert("attributes_index", "(id, attribute)", "('${{id}}', '${{name}}')", false);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{table}}", table);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{name}}", x.key());
    try {
      if (type == "boolean")
        pgsqlcmd = replaceAllBoolInstances(pgsqlcmd, "${{value}}", (bool) x.value());
      else if (type == "integer")
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", std::to_string((long) x.value()));
      else if (type == "numeric")
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", std::to_string((double) x.value()));
      else
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", "null");
      pgsqlfullcmd += pgsqlcmd;
    } catch (...) {}
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_addPlugs(std::string id, nlohmann::json &plugs)
{
  std::string pgsqlfullcmd, pgsqlcmd, updates;
  for (int i = 0; i < plugs.size(); ++i) {
    updates.clear();
    if ( !hasParameter(&plugs[i], "name") || (plugs[i]["name"].type() != nlohmann::json::value_t::string) ) continue;
    if ( !hasParameter(&plugs[i], "ord") || !ClassesImporter::isNumber(plugs[i]["ord"]) ) continue;
    if ( !hasParameter(&plugs[i], "slot_type") || (plugs[i]["slot_type"].type() != nlohmann::json::value_t::string) ) continue;
    if ( hasParameter(&plugs[i], "element") && (plugs[i]["element"].type() == nlohmann::json::value_t::string) )
      PG_addUpdateKey(updates, "element", plugs[i]["element"].get<std::string>(), true);
    if ( hasParameter(&plugs[i], "value") && ClassesImporter::isNumber(plugs[i]["value"]) )
      PG_addUpdateKey(updates, "value", std::to_string((double) plugs[i]["value"]), false);
    if ( hasParameter(&plugs[i], "mass") && ClassesImporter::isNumber(plugs[i]["mass"]) )
      PG_addUpdateKey(updates, "mass", std::to_string((double) plugs[i]["mass"]), false);
    pgsqlcmd = PG_buildInsert("plugs", "(id, name, ord, slot_type)", "\n  ('${{id}}', '${{name}}', ${{ord}}, '${{slot_type}}')", true);
    pgsqlcmd += PG_buildUpdate("plugs", updates, "where id='${{id}}' and ord=${{ord}} and slot_type='${{slot_type}}'");
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{name}}", plugs[i]["name"]);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{ord}}", std::to_string((long) plugs[i]["ord"]));
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{slot_type}}", plugs[i]["slot_type"]);
    pgsqlfullcmd += pgsqlcmd;
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_addSlots(std::string id, nlohmann::json &slots_array)
{
  std::string pgsqlfullcmd, pgsqlcmd, updates;
  for (int i = 0; i < slots_array.size(); ++i) {
    updates.clear();
    if ( !hasParameter(&slots_array[i], "name") || (slots_array[i]["name"].type() != nlohmann::json::value_t::string) ) continue;
    if ( !hasParameter(&slots_array[i], "ord") || !ClassesImporter::isNumber(slots_array[i]["ord"]) ) continue;
    if ( !hasParameter(&slots_array[i], "slot_type") || (slots_array[i]["slot_type"].type() != nlohmann::json::value_t::string) ) continue;
    if ( hasParameter(&slots_array[i], "element") && (slots_array[i]["element"].type() == nlohmann::json::value_t::string) )
      PG_addUpdateKey(updates, "element", slots_array[i]["element"].get<std::string>(), true);
    if ( hasParameter(&slots_array[i], "value") && ClassesImporter::isNumber(slots_array[i]["value"]) )
      PG_addUpdateKey(updates, "value", std::to_string((double) slots_array[i]["value"]), false);
    if ( hasParameter(&slots_array[i], "mass") && ClassesImporter::isNumber(slots_array[i]["mass"]) )
      PG_addUpdateKey(updates, "mass", std::to_string((double) slots_array[i]["mass"]), false);
    if ( hasParameter(&slots_array[i], "closure") && ClassesImporter::isNumber(slots_array[i]["closure"]) )
      PG_addUpdateKey(updates, "closure", std::to_string((double) slots_array[i]["closure"]), false);
    pgsqlcmd = PG_buildInsert("slots", "(id, name, ord, slot_type)", "('${{id}}', '${{name}}', ${{ord}}, '${{slot_type}}')", true);
    pgsqlcmd += PG_buildUpdate("slots", updates, "where id='${{id}}' and ord=${{ord}} and slot_type='${{slot_type}}'");
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{name}}", slots_array[i]["name"]);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{ord}}", std::to_string((long) slots_array[i]["ord"]));
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{slot_type}}", slots_array[i]["slot_type"]);
    pgsqlfullcmd += pgsqlcmd;
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_addSlices(std::string id, nlohmann::json &slices)
{
  std::string pgsqlfullcmd, pgsqlcmd, keys, values;
  for (int i = 0; i < slices.size(); ++i) {
    keys.clear();
    values.clear();
    if ( !hasParameter(&slices[i], "element") || (slices[i]["element"].type() != nlohmann::json::value_t::string) ) continue;
    if ( !hasParameter(&slices[i], "mass") || !ClassesImporter::isNumber(slices[i]["mass"]) ) continue;
    PG_addInsertKey(keys, values, "id", id, true);
    PG_addInsertKey(keys, values, "element", slices[i]["element"], true);
    PG_addInsertKey(keys, values, "mass", std::to_string((double) slices[i]["mass"]), false);
    if ( hasParameter(&slices[i], "name") && (slices[i]["name"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "name", slices[i]["name"], true);
    pgsqlcmd = PG_buildInsert("slices", keys, values, true);
    pgsqlfullcmd += pgsqlcmd;
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_addTriggers(std::string id, nlohmann::json &triggers)
{
  std::string pgsqlfullcmd, pgsqlcmd, new_id, updates, log_message;
  for (int i = 0; i < triggers.size(); ++i) {
    updates.clear();
    if ( !hasParameter(&triggers[i], "label") || (triggers[i]["label"].type() != nlohmann::json::value_t::string) ) {
      log_message = std::string("Trigger ") + std::to_string(i) + "from class ID " + id + " has no label. Skipping.";
      PG_log(log_message, "warning");
      continue;
    }
    PG_addUpdateKey(updates, "label", triggers[i]["label"], true);
    PG_addUpdateKey(updates, "target", id, true);
    if ( hasParameter(&triggers[i], "task") && (triggers[i]["task"].type() == nlohmann::json::value_t::string) )
      PG_addUpdateKey(updates, "task", triggers[i]["task"], true);
    if ( hasParameter(&triggers[i], "type") && (triggers[i]["type"].type() == nlohmann::json::value_t::string) )
      PG_addUpdateKey(updates, "type", triggers[i]["type"], true);
    if (!hasParameter(&triggers[i], "conditions")) triggers[i]["conditions"] = nlohmann::json::parse("[]");
    new_id = PG_newID();
    if (new_id.empty()) break;
    pgsqlcmd = PG_buildUpdate("everything", "type='trigger',in_table='triggers'", "where id='${{id}}'");
    if (hasParameter(&triggers[i], "tags"))
      pgsqlcmd += PG_addTags(new_id, "triggers", triggers[i]["tags"]);
    else {
      log_message = std::string("Trigger ") + std::to_string(i) + "from class ID " + id + " has no tags.";
      PG_log(log_message);
    }
    pgsqlcmd += PG_buildUpdate("triggers", updates, "where id='${{id}}'");
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", new_id);
    pgsqlcmd += PG_addConditions(new_id, triggers[i]["conditions"]);
    pgsqlfullcmd += pgsqlcmd;
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_addConditions(std::string id, nlohmann::json &conditions)
{
  std::string pgsqlfullcmd, pgsqlcmd, keys, values, new_id, log_message;
  for (int i = 0; i < conditions.size(); ++i) {
    keys.clear();
    values.clear();
    if ( !hasParameter(&conditions[i], "label") || (conditions[i]["label"].type() != nlohmann::json::value_t::string) ) {
      log_message = std::string("Condition ") + std::to_string(i) + "from trigger ID " + id + " has no label. Skipping.";
      PG_log(log_message, "warning");
      continue;
    }
    new_id = PG_newID();
    if (new_id.empty()) break;
    PG_addInsertKey(keys, values, "id", new_id, true);
    PG_addInsertKey(keys, values, "target", id, true);
    PG_addInsertKey(keys, values, "ord", std::to_string((int) (i+1)), false);
    PG_addInsertKey(keys, values, "label", conditions[i]["label"], true);
    if ( hasParameter(&conditions[i], "query_name") && (conditions[i]["query_name"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "query_name", conditions[i]["query_name"], true);
    if ( hasParameter(&conditions[i], "test_name") && (conditions[i]["test_name"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "test_name", conditions[i]["test_name"], true);
    if ( hasParameter(&conditions[i], "arg") && (conditions[i]["arg"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "arg", conditions[i]["arg"], true);
    if ( hasParameter(&conditions[i], "write") && (conditions[i]["write"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "write", conditions[i]["write"], true);
    if ( hasParameter(&conditions[i], "type") && (conditions[i]["type"].type() == nlohmann::json::value_t::string) )
      PG_addInsertKey(keys, values, "type", conditions[i]["type"], true);
    if ( hasParameter(&conditions[i], "value") && ClassesImporter::isNumber(conditions[i]["value"]) )
      PG_addInsertKey(keys, values, "value", std::to_string((double) conditions[i]["value"]), false);
    if ( hasParameter(&conditions[i], "amount") && ClassesImporter::isNumber(conditions[i]["amount"]) )
      PG_addInsertKey(keys, values, "amount", std::to_string((double) conditions[i]["amount"]), false);
    if (hasParameter(&conditions[i], "tags"))
      PG_addInsertKey(keys, values, "tags", PG_buildArray(conditions[i]["tags"], true), false);
    pgsqlcmd = PG_buildUpdate("everything", "type='condition',in_table='conditions'", "where id='${{id}}'");
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", new_id);
    pgsqlcmd += PG_buildInsert("conditions", keys, values, true);
    pgsqlfullcmd += pgsqlcmd;
  }
  return pgsqlfullcmd;
}

std::string ClassesImporter::PG_newID()
{
  std::string new_id;
  PGconn *conn = PQconnectdb(dboptions.c_str());
  PGresult *res = PQexec(conn, ask_id_query.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) {
    if (PQntuples(res) && PQnfields(res)) new_id = PQgetvalue(res, 0, 0);
    PQclear(res);
  }
  PQfinish(conn);
  return new_id;
}

void ClassesImporter::PG_log(std::string log_message, std::string log_level)
{
  PGconn *conn = PQconnectdb(dboptions.c_str());
  std::string log_pgsqlcmd = std::string("select log_event('") + log_level + "', 'classes_importer', '" + log_message + "');\n";
  log_pgsqlcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n" + log_pgsqlcmd + "commit;\n";
  PGresult *res = PQexec(conn, log_pgsqlcmd.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) PQclear(res);
  PQfinish(conn);
}

void ClassesImporter::PG_addInsertKey(std::string &keys, std::string &values, std::string key, std::string value, bool quoted)
{
  if (keys.empty() || values.empty()) {
    keys = "(";
    values = "(";
  }
  else {
    keys.erase(keys.end()-1);
    values.erase(values.end()-1);
    keys += ",";
    values += ",";
  }
  keys += key + ")";
  if (quoted) values += "'";
  values += value;
  if (quoted) values += "'";
  values += ")";
  return;
}

std::string ClassesImporter::PG_buildInsert(std::string table, std::string keys, std::string values, bool conflicts)
{
  std::string insert = std::string("insert into ") + table + " " + keys + " values " + values;
  if (conflicts) insert += "\n  on conflict do nothing";
  insert += ";\n";
  return insert;
}

void ClassesImporter::PG_addUpdateKey(std::string &values, std::string key, std::string value, bool quoted)
{
  if (!values.empty()) values += ",";
  values += key + "=";
  if (quoted) values += "'";
  values += value;
  if (quoted) values += "'";
  return;
}

std::string ClassesImporter::PG_buildUpdate(std::string table, std::string values, std::string where)
{
  if (values.empty()) return "";
  return std::string("update ") + table + " set " + values + "\n  " + where + ";\n";
}

std::string ClassesImporter::PG_buildArray(nlohmann::json &array, bool tags)
{
  if ( (array.type() != nlohmann::json::value_t::array) || (array.size() == 0) ||
    (array[0].type() != nlohmann::json::value_t::string) ) return "'{}'";
  std::string value = "'{";
  for (int i = 0; i < array.size(); ++i) {
    value += "\"" + array[i].get<std::string>() + "\",";
    if (tags) all_tags[array[i].get<std::string>()] = true;
  }
  value.erase(value.end()-1);
  value += "}'";
  return value;
}

std::string ClassesImporter::getAttributeTable(std::string attribute)
{
  if (hasParameter(&attributes, attribute)) return attributes[attribute]["table"];
  return "";
}

std::string ClassesImporter::getAttributeType(std::string attribute)
{
  if (hasParameter(&attributes, attribute)) return attributes[attribute]["type"];
  return "";
}

bool ClassesImporter::isNumber(nlohmann::json &j)
{
  if (j.type() == nlohmann::json::value_t::number_unsigned ||
    j.type() == nlohmann::json::value_t::number_integer ||
    j.type() == nlohmann::json::value_t::number_float)
    return true;
  else
    return false;
}

std::string ClassesImporter::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

std::string ClassesImporter::replaceAllBoolInstances(std::string string, std::string grab, bool replace_boolean)
{
  int n = 0;
  std::string replace;
  if (replace_boolean) replace = "true"; else replace = "false";
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}
