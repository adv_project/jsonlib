/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/ObjectsImporter.h"

#define MAX_QUERIES 64

ObjectsImporter::ObjectsImporter(std::string objects_directory, std::string dboptions, std::string gamename)
{
  nlohmann::json attr;
  this->dboptions = dboptions;
  this->objects_directory = objects_directory;
  this->gamename = gamename;
  ask_id_query = std::string("set search_path to ") + gamename + "; select * from make();";
  std::string attrs_query = std::string("set search_path to ") + gamename + ";\n";
  attrs_query += "select distinct(e.id), i.data_type, e.in_table from\n";
  attrs_query += "everything e inner join information_schema.columns i\n";
  attrs_query += "on i.column_name = e.id and e.type = 'attribute';";
  PGconn* conn = PQconnectdb(dboptions.c_str());
  PGresult* res = PQexec(conn, attrs_query.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) {
    int nrows = PQntuples(res);
    for(int i = 0; i < nrows; ++i) {
      attr.clear();
      attr["type"] = PQgetvalue(res, i, 1);
      attr["table"] = PQgetvalue(res, i, 2);
      attributes[PQgetvalue(res, i, 0)] = attr;
    }
    PQclear(res);
  }
  PQfinish(conn);
}

// Read all objects found at the current working directory and import them into the database
// Returns the number of imported objects
void ObjectsImporter::importObjects()
{
  nlohmann::json j, k;
  std::ifstream json_file;
  nlinks = 0;
  PGconn* conn = PQconnectdb(dboptions.c_str());;
  PGresult* res;
  std::string id, pgsqlcmd, pgsqlfullcmd;
  struct stat s;
  DIR *dir;
  struct dirent *entry;
  ids = nlohmann::json::parse("[]");
  nqueries = 0;
  pgsqlfullcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
  if (stat(objects_directory.c_str(), &s) == 0)
    if (s.st_mode & S_IFDIR) {
      if ((dir = opendir(objects_directory.c_str())) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
          if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
          if ( (stat(std::string(objects_directory + "/" + entry->d_name).c_str(), &s) == 0) && (s.st_mode & S_IFDIR) ) continue;
          PG_log("Reading objects JSON " + objects_directory + "/" + entry->d_name);
          json_file.open(std::string(objects_directory + "/" + entry->d_name).c_str());
          if (json_file.good()) {
            j.clear();
            try { json_file >> j; } catch (...) {
              PG_log("Invalid objects JSON " + objects_directory + "/" + entry->d_name, "error");
              json_file.close();
              continue;
            }
            json_file.close();
            if (j.type() != nlohmann::json::value_t::array) {
              k = j;
              j = nlohmann::json::parse("[]");
              j[0] = k;
            }
            while (j.size() > 0) {
              if ( !hasParameter(&j[0], "id") || (j[0]["id"].type() != nlohmann::json::value_t::string) ) {
                PG_log("Object " + objects_directory + "/" + entry->d_name + " doesn't have a valid ID. Ignoring.", "error");
                j.erase(0);
                continue;
              }
              id = j[0]["id"].get<std::string>();
              objects[id] = j[0];
              j.erase(0);
              ids.push_back(id);
              res = PQexec(conn, ask_id_query.c_str());
              if (PQresultStatus(res) == PGRES_TUPLES_OK) {
                if (PQntuples(res) && PQnfields(res)) objects[id]["new_id"] = PQgetvalue(res, 0, 0);
                PQclear(res);
              }
              pgsqlcmd = PG_buildUpdate("everything", "type='object', in_table='objects'", "where id='${{id}}'");
              pgsqlcmd += PG_buildInsert("objects", "(id, active)", "('${{id}}', false)", false);
              pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", objects[id]["new_id"]);
              pgsqlfullcmd += pgsqlcmd;
              ++nqueries;
              if (nqueries >= MAX_QUERIES) {
                pgsqlfullcmd += "commit;\n";
                executeQuery(conn, pgsqlfullcmd);
                pgsqlfullcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
                nqueries = 0;
              }
            }
          }
          else {
            PG_log("Couldn't read objects file " + objects_directory + "/" + entry->d_name, "error");
          }
        }
        closedir(dir);
      }
    }
    else {
      PG_log("Reading objects JSON " + objects_directory);
      json_file.open(objects_directory.c_str());
      if (json_file.good() && json_file.peek() != std::ifstream::traits_type::eof()) {
        j.clear();
        try { json_file >> j; } catch (...) {
          PG_log("Invalid objects JSON " + objects_directory, "error");
        }
        json_file.close();
        // Load JSON file into the objects list
        if (j.type() != nlohmann::json::value_t::array) {
          k = j;
          j = nlohmann::json::parse("[]");
          j[0] = k;
        }
        while (j.size() > 0) {
          if ( !hasParameter(&j[0], "id") || (j[0]["id"].type() != nlohmann::json::value_t::string) ) {
            j.erase(0);
            continue;
          }
          id = j[0]["id"].get<std::string>();
          objects[id] = j[0];
          j.erase(0);
          ids.push_back(id);
          res = PQexec(conn, ask_id_query.c_str());
          if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            if (PQntuples(res) && PQnfields(res)) objects[id]["new_id"] = PQgetvalue(res, 0, 0);
            PQclear(res);
          }
          pgsqlcmd = PG_buildUpdate("everything", "type='object', in_table='objects'", "where id='${{id}}'");
          pgsqlcmd += PG_buildInsert("objects", "(id, active)", "('${{id}}', false)", false);
          pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", objects[id]["new_id"]);
          pgsqlfullcmd += pgsqlcmd;
          ++nqueries;
          if (nqueries >= MAX_QUERIES) {
            pgsqlfullcmd += "commit;\n";
            executeQuery(conn, pgsqlfullcmd);
            pgsqlfullcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
            nqueries = 0;
          }
        }
      }
      else {
        PG_log("Couldn't read objects file " + objects_directory, "error");
      }
    }
  // Fill the everything table
  if (nqueries > 0) {
    pgsqlfullcmd += "commit;\n";
    executeQuery(conn, pgsqlfullcmd);
  }
  // Process objects
  pgsqlfullcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
  for (int i = 0; i < ids.size(); ++i) {
    if ( hasParameter(&objects[ids[i].get<std::string>()], "classes") && (objects[ids[i].get<std::string>()]["classes"].type() == nlohmann::json::value_t::array) ) {
      pgsqlfullcmd += PG_addClasses(objects[ids[i].get<std::string>()]["new_id"], "objects", objects[ids[i].get<std::string>()]["classes"]);
      ++nqueries;
    }
    if ( hasParameter(&objects[ids[i].get<std::string>()], "attributes") && (objects[ids[i].get<std::string>()]["attributes"].type() == nlohmann::json::value_t::object) ) {
      pgsqlfullcmd += PG_setAttributes(objects[ids[i].get<std::string>()]["new_id"], objects[ids[i].get<std::string>()]["attributes"]);
      ++nqueries;
    }
    if ( hasParameter(&objects[ids[i].get<std::string>()], "links") && (objects[ids[i].get<std::string>()]["links"].type() == nlohmann::json::value_t::array) ) {
      pgsqlfullcmd += PG_addLinks(ids[i], objects[ids[i].get<std::string>()]["links"]);
      ++nqueries;
    }
    if (nqueries >= MAX_QUERIES) {
      pgsqlfullcmd += "commit;\n";
      executeQuery(conn, pgsqlfullcmd);
      pgsqlfullcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n";
      nqueries = 0;
    }
  }
  pgsqlfullcmd += "commit;\n";
  executeQuery(conn, pgsqlfullcmd);
  PQfinish(conn);
  pgsqlcmd = "Imported ${{nobjects}} objects implementing ${{nclasses}} classes and having ${{nlinks}} links.";
  unsigned long nclasses = 0;
  for (auto& x : nlohmann::json::iterator_wrapper(all_classes)) ++nclasses;
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{nobjects}}", std::to_string(ids.size()));
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{nclasses}}", std::to_string(nclasses));
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{nlinks}}", std::to_string(nlinks));
  PG_log(pgsqlcmd);
  return;
}

void ObjectsImporter::executeQuery(PGconn* conn, std::string &query)
{
  PGresult* res = PQexec(conn, query.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) PQclear(res);
}

std::string ObjectsImporter::PG_addClasses(std::string id, std::string table, nlohmann::json &classes)
{
  std::string pgsqlcmd = PG_buildUpdate(table, "classes=${{classes}}", "where id='${{id}}'");
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
  pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{classes}}", PG_buildArray(classes, true));
  return pgsqlcmd;
}

std::string ObjectsImporter::PG_setAttributes(std::string id, nlohmann::json &attributes)
{
  std::string pgsqlfullcmd, pgsqlcmd, type, table;
  for (auto& x : nlohmann::json::iterator_wrapper(attributes)) {
    type = getAttributeType(x.key());
    table = getAttributeTable(x.key());
    if (type.empty() || table.empty()) continue;
    pgsqlcmd = PG_buildInsert("${{table}}", "(id)", "('${{id}}')", true);
    pgsqlcmd += PG_buildUpdate("${{table}}", "${{name}}=${{value}}", "where id='${{id}}'");
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{id}}", id);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{table}}", table);
    pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{name}}", x.key());
    try {
      if (type == "boolean")
        pgsqlcmd = replaceAllBoolInstances(pgsqlcmd, "${{value}}", (bool) x.value());
      else if (type == "integer")
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", std::to_string((long) x.value()));
      else if (type == "numeric")
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", std::to_string((double) x.value()));
      else
        pgsqlcmd = replaceAllInstances(pgsqlcmd, "${{value}}", "null");
      pgsqlfullcmd += pgsqlcmd;
    } catch (...) {}
  }
  return pgsqlfullcmd;
}

std::string ObjectsImporter::PG_addLinks(std::string id, nlohmann::json &links)
{
  std::string pgsqlfullcmd, pgsqlcmd, keys, values;
  for (int i = 0; i < links.size(); ++i) {
    keys.clear();
    values.clear();
    if ( !hasParameter(&links[i], "type") || (links[i]["type"].type() != nlohmann::json::value_t::string) ) continue;
    if ( !hasParameter(&links[i], "target") || (links[i]["target"].type() != nlohmann::json::value_t::string) ) continue;
    PG_addInsertKey(keys, values, "id", objects[id]["new_id"], true);
    PG_addInsertKey(keys, values, "type", links[i]["type"], true);
    PG_addInsertKey(keys, values, "target", objects[links[i]["target"].get<std::string>()]["new_id"], true);
    if ( hasParameter(&links[i], "name") && (links[i]["name"].type() == nlohmann::json::value_t::string) ) 
      PG_addInsertKey(keys, values, "name", links[i]["name"], true);
    pgsqlcmd = PG_buildInsert("links", keys, values, true);
    pgsqlfullcmd += pgsqlcmd;
    ++nlinks;
  }
  return pgsqlfullcmd;
}

void ObjectsImporter::PG_log(std::string log_message, std::string log_level)
{
  PGconn *conn = PQconnectdb(dboptions.c_str());
  std::string log_pgsqlcmd = std::string("select log_event('") + log_level + "', 'objects_importer', '" + log_message + "');\n";
  log_pgsqlcmd = std::string("set search_path to ") + gamename + ";\nbegin;\n" + log_pgsqlcmd + "commit;\n";
  PGresult *res = PQexec(conn, log_pgsqlcmd.c_str());
  if (PQresultStatus(res) == PGRES_TUPLES_OK) PQclear(res);
  PQfinish(conn);
}

void ObjectsImporter::PG_addInsertKey(std::string &keys, std::string &values, std::string key, std::string value, bool quoted)
{
  if (keys.empty() || values.empty()) {
    keys = "(";
    values = "(";
  }
  else {
    keys.erase(keys.end()-1);
    values.erase(values.end()-1);
    keys += ",";
    values += ",";
  }
  keys += key + ")";
  if (quoted) values += "'";
  values += value;
  if (quoted) values += "'";
  values += ")";
  return;
}

std::string ObjectsImporter::PG_buildInsert(std::string table, std::string keys, std::string values, bool conflicts)
{
  std::string insert = std::string("insert into ") + table + " " + keys + " values " + values;
  if (conflicts) insert += "\n  on conflict do nothing";
  insert += ";\n";
  return insert;
}

void ObjectsImporter::PG_addUpdateKey(std::string &values, std::string key, std::string value, bool quoted)
{
  if (!values.empty()) values += ",";
  values += key + "=";
  if (quoted) values += "'";
  values += value;
  if (quoted) values += "'";
  return;
}

std::string ObjectsImporter::PG_buildUpdate(std::string table, std::string values, std::string where)
{
  if (values.empty()) return "";
  return std::string("update ") + table + " set " + values + "\n  " + where + ";\n";
}

std::string ObjectsImporter::PG_buildArray(nlohmann::json &array, bool classes)
{
  if ( (array.type() != nlohmann::json::value_t::array) || (array.size() == 0) ||
    (array[0].type() != nlohmann::json::value_t::string) ) return "'{}'";
  std::string value = "'{";
  for (int i = 0; i < array.size(); ++i) {
    value += "\"" + array[i].get<std::string>() + "\",";
    if (classes) all_classes[array[i].get<std::string>()] = true;
  }
  value.erase(value.end()-1);
  value += "}'";
  return value;
}

std::string ObjectsImporter::getAttributeTable(std::string attribute)
{
  if (hasParameter(&attributes, attribute)) return attributes[attribute]["table"];
  return "";
}

std::string ObjectsImporter::getAttributeType(std::string attribute)
{
  if (hasParameter(&attributes, attribute)) return attributes[attribute]["type"];
  return "";
}

std::string ObjectsImporter::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

std::string ObjectsImporter::replaceAllBoolInstances(std::string string, std::string grab, bool bool_replace)
{
  int n = 0;
  std::string replace;
  if (bool_replace) replace = "true"; else replace = "false";
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}
