/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/AdvInput.h"

AdvInput::AdvInput(int cols)
{
  prompt = L"> ";
  cursor = 1;
  insert = false;
  setCols(cols);
  history_position = 0;
}

wchar_t AdvInput::getChar()
{
  wint_t key_int;
  int ret = get_wch(&key_int);
  if (ret == ERR) return ERR;
  int cmdcols = cols - prompt.length();
  wchar_t key = (wchar_t) key_int;
  switch (key) {
    case 3: case 13: case 26: case 27: case 28: case 29: case 30: case 31:
      return ERR;
    case KEY_F(1): case KEY_F(2): case KEY_F(3): case KEY_F(4):
    case KEY_F(5): case KEY_F(6): case KEY_F(7): case KEY_F(8):
    case KEY_F(9): case KEY_F(10): case KEY_F(11): case KEY_F(12):
    case KEY_RESIZE: case '\n':
    case CTRL('b'): case CTRL('n'): case CTRL('l'):
    case CTRL('q'): case CTRL('w'): case CTRL('e'): case CTRL('r'): case CTRL('t'):
    case CTRL('y'): case CTRL('u'): case CTRL('o'): case CTRL('p'):
    case CTRL('a'): case CTRL('s'): case CTRL('d'): case CTRL('f'): case CTRL('g'):
    case CTRL('k'): case CTRL('x'): case CTRL('v'): case CTRL(' '):
    case KEY_PPAGE: case KEY_NPAGE: case 9: // Tab
      break;
    case KEY_UP:
      if ( (history_position == 0) && (history.size() > 0) ) cmdline_backup = cmdline;
      ++history_position;
      if (history_position < 0) history_position = 0;
      if (history_position > history.size()) history_position = history.size();
      if (history_position > 0) {
        cmdline = history.at(history.size() - history_position);
        if (cmdline.length() > (cmdcols - 2)) {
          cursor = cmdcols - 1;
          offset = cmdline.length() - cmdcols + 2;
        }
        else {
          cursor = cmdline.length() + 1;
          offset = 0;
        }
      }
      break;
    case KEY_DOWN:
      --history_position;
      if (history_position == 0) {
        cmdline = cmdline_backup;
        if (cmdline.length() > (cmdcols - 2)) {
          cursor = cmdcols - 1;
          offset = cmdline.length() - cmdcols + 2;
        }
        else {
          cursor = cmdline.length() + 1;
          offset = 0;
        }
      }
      else {
        if (history_position < 0) history_position = 0;
        if (history_position > history.size()) history_position = history.size();
        if (history_position > 0) {
          cmdline = history.at(history.size() - history_position);
          if (cmdline.length() > (cmdcols - 2)) {
            cursor = cmdcols - 1;
            offset = cmdline.length() - cmdcols + 2;
          }
          else {
            cursor = cmdline.length() + 1;
            offset = 0;
          }
        }
      }
      break;
    case KEY_LEFT:
      if (cursor > 1) {
        if ( (cursor < 3) && (offset > 0) ) --offset; else --cursor;
      }
      else if (offset > 0) --offset;
      break;
    case KEY_RIGHT:
      if ((cursor + offset) < (cmdline.length() + 1)) {
        ++cursor;
        if (cursor > (cmdcols - 1)) {
          cursor = cmdcols - 1;
          ++offset;
        }
      }
      break;
    case KEY_BACKSPACE:
    case 0x7f:
      if (cursor > 1) {
        cmdline.erase(cursor + offset - 2, 1);
        if ( (cursor < 3) && (offset > 0) ) --offset; else --cursor;
      }
      else if (offset > 0) {
        cmdline.erase(cursor + offset - 2, 1);
        --offset;
      }
      break;
    case KEY_DC:
      cmdline.erase(cursor - 1, 1);
      break;
    case KEY_IC:
      insert = !insert;
      break;
    case KEY_HOME:
      cursor = 1;
      offset = 0;
      break;
    case KEY_END:
      if (cmdline.length() > (cmdcols - 2)) {
        cursor = cmdcols - 1;
        offset = cmdline.length() - cmdcols + 2;
      }
      else {
        cursor = cmdline.length() + 1;
        offset = 0;
      }
      break;
    default:
      if (cmdline.length() >= (cursor + offset)) {
        if (insert) cmdline.erase(cursor + offset - 1, 1);
        cmdline = cmdline.substr(0, cursor + offset - 1) + key +
          cmdline.substr(cursor + offset - 1);
      }
      else cmdline = cmdline + key;
      ++cursor;
      if (cursor > cmdcols) {
        cursor = cmdcols;
        ++offset;
      }
      break;
  }
  return key;
}

bool AdvInput::handleCompletion()
{
  unsigned int index;
  bool found = false;
  for (index = last_completion; index < history.size(); ++index)
    if (history[history.size() - index - 1].substr(0, completion.length()) == completion) {
      found = true;
      break;
    }
  if (found) {
    last_completion = index;
    cmdline = history[history.size() - last_completion - 1];
    last_completion++;
    int cmdcols = cols - prompt.length();
    if (cmdline.length() > (cmdcols - 2)) {
      cursor = cmdcols - 1;
      offset = cmdline.length() - cmdcols + 2;
    }
    else {
      cursor = cmdline.length() + 1;
      offset = 0;
    }
  }
  else
    last_completion = 0;
  return found;
}

void AdvInput::setCols(int cols)
{
  this->cols = cols;
  offset = 0;
}
