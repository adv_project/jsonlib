/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include "../include/ClassesImporter.h"

int main(int argc, char **argv)
{
  std::string dboptions, gamename;
  if (argc > 1) dboptions = argv[1]; else return 1; // Grab PostgreSQL connection options or abort
  if (argc > 2) gamename = argv[2]; else return 2; // Read game name from argv[2]
  if (argc > 3) chdir(argv[3]); // Change to the directory provided through argv[3]
  ClassesImporter CI(dboptions, gamename);
  CI.importClasses();
  return 0;
}
