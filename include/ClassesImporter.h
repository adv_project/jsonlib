/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLASSESIMPORTER_H
#define CLASSESIMPORTER_H

#include <dirent.h>
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include "json.hpp"

#ifndef POSTGRESQLDIR
#include <libpq-fe.h>
#else
#include <postgresql/libpq-fe.h>
#endif

class ClassesImporter
{
  public:
    ClassesImporter(std::string dboptions, std::string gamename);
    void importClasses();
    std::string getAttributeTable(std::string attribute);
    std::string getAttributeType(std::string attribute);
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }
    static bool isNumber(nlohmann::json &j);
    static std::string replaceAllInstances(std::string string,
                                           std::string grab, std::string replace);
    static std::string replaceAllBoolInstances(std::string string,
                                           std::string grab, bool replace_boolean);

  protected:
    std::string PG_addAnything(std::string id, std::string type, std::string in_table);
    std::string PG_addClasses(std::string id, std::string table, nlohmann::json &classes);
    std::string PG_addTags(std::string id, std::string table, nlohmann::json &tags);
    std::string PG_addAttributes(std::string name, nlohmann::json &attributes);
    std::string PG_addPlugs(std::string id, nlohmann::json &plugs);
    std::string PG_addSlots(std::string id, nlohmann::json &slots_array);
    std::string PG_addSlices(std::string id, nlohmann::json &slices);
    std::string PG_addTriggers(std::string id, nlohmann::json &triggers);
    std::string PG_addConditions(std::string id, nlohmann::json &conditions);

    std::string PG_newID();
    void PG_log(std::string log_message, std::string log_level = "notice");
    void PG_addInsertKey(std::string &keys, std::string &values, std::string key, std::string value, bool quoted);
    std::string PG_buildInsert(std::string table, std::string keys, std::string values, bool conflicts);
    void PG_addUpdateKey(std::string &values, std::string key, std::string value, bool quoted);
    std::string PG_buildUpdate(std::string table, std::string values, std::string where);
    std::string PG_buildArray(nlohmann::json &array, bool tags);

    std::string dboptions;
    std::string gamename;
    std::string ask_id_query;
    nlohmann::json attributes;
    nlohmann::json all_tags;
    unsigned long nclasses;
    unsigned long nobjects;
};

#endif // CLASSESIMPORTER_H
