/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JSONCREATOR_H
#define JSONCREATOR_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local
#define MAIN_DELAY 5000
#define ACTIONS_DELAY 0
#define PREVIEW_BUFFER 50000

#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <locale>
#include <boost/locale/encoding_utf.hpp>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <pthread.h>
#include "json.hpp"
#include "sigslot.h"
#include "AdvInput.h"
#include "Window.h"

class JsonCreator : public sigslot::has_slots<>
{
  public:
    JsonCreator();
    ~JsonCreator();
    int exec(int argc, char **argv);
    nlohmann::json loadMenus();
    void startUI();
    void stopUI();
    void updateView();
    void updateInputLine();
    void exit() { do_exit = true; }
    void handleInput(wchar_t key);
    void parseCommand(std::string cmd);
    void activateMenu(std::string menu);
    void buildHistory(std::string complete);
    void loadIndex();
    void loadClasses();
    void exportIndex();
    void exportClasses();
    void updateCompletions();
    void runCallback(std::string callback);
    void showCompletions();
    void showFilteredList();
    void applyFilter(std::string filter);
    void refreshFilterArray();
    std::string filterText(nlohmann::json filter);
    void showIndex();
    void showActiveJSON(unsigned long index = 0, unsigned long size = 0);
    void setActiveJSON(std::string name = "");
    void runIntegrityCheks();
    void addCheckText(std::string text, std::string type = "normal");
    void windowResized(int SIG);
    std::vector<std::string> dumpIndex();
    std::vector<std::string> dumpJSON();
    static std::string replaceAllInstances(std::string string,
                                           std::string grab, std::string replace);
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }

  protected:
    void checkDuplicatedSlugName(std::string slug);
    void checkDuplicatedTriggerLabel();
    void checkDuplicatedConditionLabel();
    void checkDuplicatedInitLabel();

    void callbackChangeDirectory();
    void callbackListProperties();
    void callbackSelectIndex();
    void callbackCreateClass();
    void callbackCopyClass();
    void callbackMoveClass();
    void callbackDeleteClass();
    void callbackDeleteClassMultiple();
    void callbackSetType();
    void callbackSetTypeMultiple();
    void callbackRemoveType();
    void callbackRemoveTypeMultiple();
    void callbackAddToArray(std::string field_name, std::string completion_name);
    void callbackAddToArrayMultiple(std::string field_name, std::string completion_name);
    void callbackRemoveFromArray(std::string field_name, std::string completion_name);
    void callbackRemoveFromArrayMultiple(std::string field_name, std::string completion_name);
    void callbackSetModName();
    void callbackSelectAttribute();
    void callbackSetAttributeField(std::string field_name, std::string completion_name);
    void callbackRemoveIndexAttribute();
    void callbackSetBoundary();
    void callbackSetBoundaryValue();
    void callbackSetAttribute(bool multiple);
    void callbackSetAttributeValue();
    void callbackSetAttributeValueMultiple();
    void callbackRemoveAttribute();
    void callbackRemoveAttributeMultiple();
    void callbackSelectSlug(std::string field_name, std::string completion_name);
    void callbackSelectSlugMultiple(std::string field_name, std::string completion_name);
    void callbackEditSlugName(std::string slug, std::string completion_name);
    void callbackSetSlugField(std::string slug, std::string field_name, std::string completion_name);
    void callbackSetSlugFieldMultiple(std::string slug, std::string field_name, std::string completion_name);
    void callbackSetSlugValue(std::string slug, std::string field_name);
    void callbackSetSlugValueMultiple(std::string slug, std::string field_name);
    void callbackCopySlug(std::string slug);
    void callbackPasteSlug(std::string slug);
    void callbackRemoveSlug(std::string slug);
    void callbackRemoveSlugMultiple(std::string slug);
    void callbackSelectTrigger();
    void callbackSelectTriggerMultiple();
    void callbackEditTriggerLabel();
    void callbackSetTriggerField(std::string field_name, std::string completion_name);
    void callbackSetTriggerFieldMultiple(std::string field_name, std::string completion_name);
    void callbackRemoveTriggerType();
    void callbackRemoveTriggerTypeMultiple();
    void callbackAddTriggerTag();
    void callbackAddTriggerTagMultiple();
    void callbackRemoveTriggerTag();
    void callbackRemoveTriggerTagMultiple();
    void callbackMoveTriggerUp();
    void callbackMoveTriggerDown();
    void callbackCopyTrigger();
    void callbackPasteTrigger();
    void callbackRemoveTrigger();
    void callbackRemoveTriggerMultiple();
    void callbackSelectCondition();
    void callbackSelectConditionMultiple();
    void callbackEditConditionLabel();
    void callbackSetConditionField(std::string field_name, std::string completion_name);
    void callbackSetConditionFieldMultiple(std::string field_name, std::string completion_name);
    void callbackRemoveConditionField(std::string field_name);
    void callbackRemoveConditionFieldMultiple(std::string field_name);
    void callbackSetConditionValue(std::string field_name);
    void callbackSetConditionValueMultiple(std::string field_name);
    void callbackAddConditionTag();
    void callbackAddConditionTagMultiple();
    void callbackRemoveConditionTag();
    void callbackRemoveConditionTagMultiple();
    void callbackMoveConditionUp();
    void callbackMoveConditionDown();
    void callbackCopyCondition();
    void callbackPasteCondition();
    void callbackRemoveCondition();
    void callbackRemoveConditionMultiple();
    void callbackSelectBiomeInit();
    void callbackSelectBiomeInitMultiple();
    void callbackEditInitLabel();
    void callbackSelectBiomeInitAttribute();
    void callbackSelectBiomeInitAttributeMultiple();
    void callbackSetBiomeInitAttribute(std::string field_name);
    void callbackSetBiomeInitAttributeMultiple(std::string field_name);
    void callbackRemoveBiomeInitAttribute();
    void callbackRemoveBiomeInitAttributeMultiple();
    void callbackCopyBiomeInit();
    void callbackPasteBiomeInit();
    void callbackRemoveBiomeInit();
    void callbackRemoveBiomeInitMultiple();
    void callbackAddEcoregion();
    void callbackAddEcoregionMultiple();
    void callbackRemoveEcoregion();
    void callbackRemoveEcoregionMultiple();
    void callbackCheckJSONsIntegrity();
    void callbackRunIntegrityCheck();
    void callbackExportAll();

    std::string working_directory; // The current working directory
    std::string active_menu; // The current active menu
    std::string active_json; // The current JSON being edited
    std::string argument; // The actual argument grabbed from command line
    std::string active_attribute; // The actual attribute grabbed from command line
    std::string active_boundary; // The actual boundary grabbed from command line
    std::string active_slug; // The actual slice/slot/plug name grabbed from command line
    unsigned int active_slug_index; // The actual slice/slot/plug index inside the array
    std::string active_trigger; // The actual trigger label grabbed from command line
    unsigned int active_trigger_index; // The actual trigger index inside the array
    std::string active_condition; // The actual condition label grabbed from command line
    unsigned int active_condition_index; // The actual condition index inside the array
    std::string active_biome_init; // The actual biome init label grabbed from command line
    unsigned int active_biome_init_index; // The actual biome init index inside the array
    std::string callback; // The currently running callback
    bool do_exit, ui, new_lines_on_view, tab_mode, done,
      loaded, exported, copied, not_copied, moved, not_moved, deleted, changes_made;
    pthread_mutex_t view_mutex; // updateView() must be thread-safe
    nlohmann::json menus; // Program menus in JSON format
    nlohmann::json classes; // Loaded JSON classes
    nlohmann::json index; // Loaded index JSON file
    nlohmann::json completions; // Sets of words to autocomplete
    nlohmann::json defaults; // Default values when creating new objects
    nlohmann::json deletions; // Files to be deleted when exporting JSONs
    nlohmann::json clipboard; // Copied trigger/slug/init
    nlohmann::json filtered_list; // List of filtered JSON classes
    nlohmann::json applied_filters; // List of applied filters
    nlohmann::json filter_array; // Array of filtered JSONs
    long filter_array_index;
    std::vector<std::string> checks;
    WINDOW *inputWindow;
    Window *menuWindow;
    Window *previewWindow;
    Window *titleWindow;
    int preview_scroll; // The actual scroll state for JSON previews
    bool preview_json_changed;
    AdvInput *input; // Keyboard input handler
};

#endif // JSONCREATOR_H
