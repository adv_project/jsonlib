/*
 * This file is part of the ADV Project
 * Text-based adventure game, highly modular and customizable
 * Written by Marcelo López Minnucci <coloconazo@gmail.com>
 * and Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TABLESIMPORTER_H
#define TABLESIMPORTER_H

#include <dirent.h>
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include "json.hpp"

#ifndef POSTGRESQLDIR
#include <libpq-fe.h>
#else
#include <postgresql/libpq-fe.h>
#endif

class TablesImporter
{
  public:
    TablesImporter(std::string dboptions, std::string gamename);
    void importTables();
    static bool hasParameter(nlohmann::json *j, std::string p)
      { return ( (*j).find(p) != (*j).end() ); }
    static bool isNumber(nlohmann::json &j);
    static std::string replaceAllInstances(std::string string,
                                           std::string grab, std::string replace);
    static std::string replaceAllBoolInstances(std::string string,
                                           std::string grab, bool replace_boolean);

  protected:
    std::string PG_addAnything(std::string id, std::string type, std::string in_table);
    std::string PG_createTable(std::string name, nlohmann::json &attributes, bool is_directory);
    
    void PG_log(std::string log_message, std::string log_level = "notice");
    std::string PG_buildInsert(std::string table, std::string keys, std::string values, bool conflicts);
    std::string PG_buildUpdate(std::string table, std::string values, std::string where);

    std::string dboptions;
    std::string gamename;
    std::string attributes_query;
    unsigned long ntables, nboundaries;
};

#endif // TABLESIMPORTER_H
